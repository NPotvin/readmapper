\documentclass[assignment2_Potvin_Nicolas.tex]{subfiles}

\section{Read mapping}
RNA-seq mappers used to be extensions of DNA mappers\cite{star}. A DNA mapper based on suffix arrays would iteratively consider all suffixes of the short read. The goal is to find seeds, then extending them allowing mismatches (up to a point, limits are defined by the user).\par
In this work, the STAR algorithm has been implemented based on the previously described suffix array. It has been chosen firstly (and mainly) because rather than a DNA mapper, it is a mapper specialised for RNA. Consequently, it outperforms DNA mappers when aligning RNA in terms of map quality but also in terms of computational efficiency (which is the second motivation of that choice).

\subsection{STAR}
In the STAR\cite{star} algorithm, a first simple yet powerful observation is made: RNA is definitely not DNA, in the sense that even a (hypothetical) read of perfect quality could not find a full contiguous match in the genome due to the introns removal in the post-transcriptional phase of the RNA synthesis. This observation led to a change in the way the mapping is done. STAR first tries to map exactly the longest possible prefixes of the read instead of trying to extend seeds that could be on an edge of an exon (hence potentially introducing a biased match/mismatch sequence coming from an intron). When a mismatch is encountered, the remaining unmapped suffix is mapped in the same way (potentially to a completely different region of the genome than the prefix, jumping over a potential intron).\par
This approach has a huge advantage in terms of time since it requires far less passes on the read, contrary to classical DNA mapping (in most cases, only few passes are enough). An other consequence is that only exact matches are considered, which allows to distinguish clearly possible areas containing mismatches that could yield a lower quality (yet acceptable) map and completely messed up parts that are more likely to be thrown away due to too low quality.\par

\subsubsection{Clustering}
Sadly there is a catch to consider only "un-extended" seeds. They could map a very high number of parts of the genome, rendering the task to locate which alignment is more likely to be the right one almost impossible. To alleviate this problem, some seeds are considered special among others. Those are called "anchors", they are used as starting points in the search of possible maps. To be an anchor, a seed needs to map a number of the genome regions that is below a (user-defined) threshold.\par
Once anchors are identified among the seeds, they are used to "open windows on the genome", that is consider only the genome areas they map to, enlarged on each ends by a user-defined value. Windows reduce greatly the considered parts of the genome, yet still allow the seeds mapped in their area to be used. If two windows overlap, they are merged in a larger window (note: the anchors themselves cannot overlap since they are different parts of the read, overlap is allowed only for their "area of effect").\par
In effect these windows act as clusters that help identifying which map-seeds are worth keeping and which are most likely to be a map by mere coincidence. It is important to note that the size of the "windows-wings" (\ie the regions on each sides of anchors) should depend on the usual size of the studied genome's introns. That is due to the possible merging of close windows; if their area is too large, they could cover an intron between their two anchors.\\
\begin{algorithm}[H]
\caption{cluster}
\DontPrintSemicolon
    \KwIn{\;
        \Indp
        $R$: The read\;
        $G$: The genome (indexed)\;
        $ws$: The windows wings' size on each sides\;
        $k$: Minimum size for a seed ($k$-mers)\;
        $aMaxMaps$: Maximum number of maps to be an anchor\;
    }
    \KwOut{\;
        \Indp
        $W$: A set of windows' borders\;
        $C$: A set of clusters \ie a sparse matrix for $G\times R$\;
    }
    $W \longleftarrow \{\}$\;
    $S \longleftarrow \{\}$: a set of seeds\;
    $r \longleftarrow 0$\;
    \Repeat{$r \geq |R|$}{
        $M \longleftarrow$ longest exact match of $R_rR_{r+1}...R_{r+l}$ in $G$\;
        \uIf{$|M| \geq k$}{
          $S \longleftarrow S\cup\{M\}$\;
          \If{maps of $M$ in $G \leq aMaxMaps$}{
            \For{$m$ the maps of $M$ in $G$}{
              \uIf{$\exists w \in W$ s.t. $m \in w$}{
                extend $w$ to include $m$ and add $ws$ on the extended side\;
              }
              \Else{
                $w \longleftarrow ((m_g-ws, m_r-ws), (m_g+|M|+ws, m_r+|M|+ws))$\;
                $W \longleftarrow W\cup\{w\}$\;
              }
            }
          }
          $S \longleftarrow {m | m \in M}$: add all seeds to $S$\;
          $r \longleftarrow r+|M|$\;
        }
        \Else{
          $r \longleftarrow r+1$\;
        }
        $C \longleftarrow s \forall s\in S$ s.t. $s \in w$ for any $w \in W$\;
    }
\end{algorithm}

\subsubsection{Scoring}
In the supplementary response of the original paper\cite{star} there is a mention of a dynamic-programming method to compute the scores that could be used in a next release of STAR. The approach used in this work differs slightly from the one used originally and uses this idea of applying dynamic-programming. The scheme used is a simple Smith-Waterman local alignment applied to each window. It makes use of user-defined gap insertion/extension penalties that can be set differently when applied to gaps in the genome or in the mapped read. Following the policy of STAR, only one gap is allowed per alignment (note: "only one gap" means only one gap insertion, but potentially several extensions).\par
Even if the alignment algorithm differs slightly, the computation of the scores is the same, and the motivation behind the use of Smith-Waterman is admittedly personal curiosity first, and performance second. (The main observed consequence is a huge consumption of memory ($10\sim 12$Gb), this could be lowered by implementing a sparse matrix dedicated to this problem rather than using the more generic ones proposed by scipy).\par
Given a cluster $C$ \ie a matrix spanning a region of $G\times R$ where $G$ is the genome and $R$ the read:
$$
  V_{g, r} = \texttt{max}\left(\begin{array}{l}
    S_{g-1, r} - dop \\
    V_{g-1, r} - dep
  \end{array}\right)
$$$$
  W_{g, r} = \texttt{max}\left(\begin{array}{l}
    S_{g, r-1} - iop \\
    V_{g, r-1} - iep
  \end{array}\right)
$$$$
  S_{g, r} = \texttt{max}\left(\begin{array}{l}
    S_{g-1, r-1} + \texttt{match}(g, r) \\
    V_{g, r} \\
    W_{g, r} \\
    0
  \end{array}\right)
$$
where $dop$, $dep$, $iop$, $iep$ are the penalties for opening and extending deletions and insertions respectively (\ie the gaps in the alignment). The function computing the score of a match is defined as:
$$
\texttt{match}(g, r) = \begin{array}{|lc}
  1 & \text{if }  R_r = G_g \\
  -1 & \text{otherwise}
\end{array}
$$

\subsubsection{Stitching}
Armed with the scores, it becomes possible to recover possible alignments (inside each windows) using the backtracking phase of the Smith-Waterman algorithm. This backtracking has been tweaked a bit to forbid the use of more than one gap insertion in either of the directions (genome-wise or read-wise) as to follow the original STAR algorithm.\par
An important remark has to be made here concerning the allowed alignments. It is allowed to mismatch, to skip bases and even to introduce new ones but whatever happens, the relative order of the bases must not be disturbed. This rule comes from the assumption that the RNA transcription follows a linear transcription model, \ie during transcription, the DNA is read sequentially to build the RNA. Parts may be removed (introns) and errors can be introduced (mismatches), but a "rewind operation" is considered impossible. In the code, this rule is implicitly present since, by construction, dynamic-programming cannot "go backward" during the scoring phase (\ie the scores of each box depends on those met previously (up and/or left) but never on those met afterwards).\\
\begin{algorithm}[H]
\caption{stitch}
\DontPrintSemicolon
    \KwIn{\;
        \Indp
        $C$: The clusters\;
        $iop, iep, dop, dep$: gap penalties\;
    }
    \KwOut{\;
        \Indp
        $A$: A set of candidate alignments (maps) for each cluster\;
    }
    \For{$c \in C$}{
      compute $V$, $W$, $S$ using Smith-Waterman on $c$\;
      \For{$g, r$ coordinates of the end of alignments}{
        \tcc{\ie where $S = 0$ on the bottom right 1-cell-long area around $S_{gr}$}
        $A \longleftarrow A\cup\texttt{stitchBacktrack}(S, V, W, g, r)$\;
      }
    }
\end{algorithm}
Where $\texttt{stitchBacktrack}$ is a modified version of Smith-Waterman backtracking, defined as the following algorithm.\\
\begin{algorithm}[H]
\caption{stitchBacktrack}
\DontPrintSemicolon
    \KwIn{\;
        \Indp
        $S$, $V$, $W$: The Smith-Waterman score matrices\;
        $g, r$: The coordinates where the backtrack starts\;
        $D$: A set of directions in which it is allowed to backtrack\;
        \tcc{default value of $D = u, l$ \ie Up, Left. Diagonal is always allowed}
    }
    \KwOut{\;
        \Indp
        $A$: Candidate alignments (maps) based on the matrices\;
    }
    $A \longleftarrow \{\}$\;
    \lIf{$S_{gr} = 0$}{\Return{$A$}}
    $a \longleftarrow \{(g, r)\}$: current alignment\;
    \If{$u \in D \& W_{gr} = S_{gr}$}{
      $D' \longleftarrow D\setminus_u$\;
      \While{$W_{gr} = S_{gr}$}{
        $A' \longleftarrow \texttt{stitchBacktrack}(S, V, W, g, r-1, D')$\;
      }
      $A \longleftarrow A\cup \{a'\cup a \forall a' \in A'\}$\;
    }
    \If{$l \in D \& V_{gr} = S_{gr}$}{
      $D' \longleftarrow D\setminus_l$\;
      \While{$V_{gr} = S_{gr}$}{
        $A' \longleftarrow \texttt{stitchBacktrack}(S, V, W, g-1, r, D')$\;
      }
      $A \longleftarrow A\cup \{a'\cup a \forall a' \in A'\}$\;
    }
    \If{$S_{g, r} = S_{g-1, r-1} + \texttt{match}(g, r)$}{
      $A' \longleftarrow \texttt{stitchBacktrack}(S, V, W, g-1, r-1, D)$\;
      $A \longleftarrow A\cup \{a'\cup a \forall a' \in A'\}$\;
    }
    \Return{$A$}
\end{algorithm}

\subsection{Forward and reverse}
In STAR\cite{star} there is a mention of concurrent clustering and stitching of both the forward and reverse reads. In this work it has been attempted to implement that notion with a mitigated result. The argument is that both reads are actually the two ends of a same sequence and as such, should be treated together as a single sequence (with a possible overlap or gap of their inner ends). As a first attempt (and in order to have some results soon enough) these reads have been considered as following each other, with the parts of the reversed one following the forward one.\par
The main issue is that the alignments are not looked for independently, as they should. In the current implementation they have been made too "entangled" and finding a good alignment for one usually results in discarding alignments for the other. Fixing this requires to split windows from the forward sequence and those from the reverse sequence before the alignment selection, but time was lacking to apply that fix.

\subsection{Alignment selection}
The output of the previous phases is a set of maps that can be used to build an alignment of the read. The border between where ends the job of an Read-Mapper and where begins analysis of the maps and their use in order to compute alignments meaningful for a deeper analysis is a bit blurred. Sadly the decision on "where to stop" was taken away by the deadline and a lack of time to achieve the implementation that was wished for. In the current state of the program, given a read and a genome as input, the output is a list of "maps" \ie arrays of couples $(g, r)$ with their associated score, where $g$ is the index of a base in the genome and $r$ is the index of a base in the read. Different maps are grouped by the window they come from to ease their processing by the actual aligner (if considering an aligner as taking maps as input).\par
Nevertheless, (as explained below) naively returning the best windows already yields good alignments in most cases. (Note: the "best windows" means the best one along those having a score in a user-defined range of the best, $10\%$ range is considered in this work).\par
In the alignment phase, several types of alignments are possible. The easiest is the \textit{full-read} alignment, \ie an alignment (mostly) contiguous in the genome with few mismatches and no (or very short) insertion/deletion. An other possible alignment typical to RNA is the \textit{splice-junction}, that is a read that skips a whole part of the genome (an intron) while matching strongly donor/acceptor parts of the surrounding exons. STAR is particularly good at detecting these alignments do to its different approach of seed search/extend. An other situation is the encounter of a poly-A tail, adapter or simply a section of too low quality to be aligned. In this case, the alignment usually trim the concerned part of the read.\par
These three alignments cases are covered by the current implementation if the windows size is large enough. Contiguous alignments fit in a single window, hence returning the best ones is exactly what is required. In the splice case, given the assumption that the introns size is smaller than windows size, the returned alignments are still good, yet with a lower score than what could achieved with a smarter selection. In the case of too low quality or tail-specific mapping STAR already takes care of not considering these sections in the previous stages of the algorithm by giving these sections a too low score to be kept as candidate alignment.\par
These previous alignment types make use of only one window\cite{star}. A more difficult alignment is when the best windows covers a fraction of the read that is too small. In that case, the aligner requires to use several (lower-scored) windows and to arrange them as well as possible and to produce what is called a \textit{chimeric alignment}. Due to their "multi-windows" nature, these alignments can skip a very large part of the genome to the point of matching parts from two different chromosomes.
% TODO : examples of outputs

\subsection{Analysis}
It is assumed that the index is already built and that STAR begins its computation with a genome $G$, its index composed of a pre-index (tree-like) and a suffix array, and a set of ready-to-use short-reads.

\subsubsection{Clustering}
For each read $R$, STAR begins by the clustering phase which requires at worst time one search for each base of the read (hence not match at all, which is highly improbable). If $k$ (minimum size of a seed) is smaller than the pre-index depth, each of these searches are in $k$-linear time, hence a worst time of $O(k|R|)$. If there is no pre-index at disposal, each search requires $O(k|R|\texttt{log}(|G|))$ due to the use of binary search on the suffix array.\par
However it is important to note how pessimistic that complexity is. Finding no match actually results in early return in practice, making the detection of an unmatch linear in $k$ at worse (hence near-constant time since $k$ is fixed). In more details, the number of look-up in the index depends on the sum of matches $m$ and mismatches $\overline{m}$ which is a number below $|R|$ since a match is at least $k$ long.\par
Hence $\overline{m}+km \leq \overline{m}+lm \approx |R|$ where $l \geq k$ is the average length of matches. To ease the computation, let's assume the pre-index has a depth of $h \geq k$ (in practice it is far above usual values for $k$, hence quickening the searches even further).\par
The time becomes $\overline{m}k + mh + m(l-h)\texttt{log}(G')$.\par
$G'$ is the fraction of $G$ bounded after the search in the pre-index, for an alphabet of size $a$ where letters are assumed to be equiprobable in the genome, it has an expected value of $|G|/a^h$, hence time becomes:
$$
  \overline{m}k + mh + m(l-h)(\texttt{log}(|G|)-h)
$$
If $h \geq l$ we fall back on the linear case of a tree look-up, the higher $l-h$ gets, the more costly a look-up becomes and the optimisation of performances in this case reduces to the minimisation of the quantity $(l-h)(\texttt{log}(|G|)-h)$ which depends on the expected value of $l$ (which depends on the quality of the reads) and the quantity of memory available to store the pre-index. This expression denotes formally the trade-off speed-memory that can be made during the clustering.\par
The length of a match can be expressed as a random variable following a geometric distribution (the number of matched bases before the first mismatch). Expressed using the probability "the observed base is an error" $p$, $(1-p)^lp$ is the probability to match exactly $l$ bases.\par
Matching a part of the read that do not require a look beyond the pre-index has a probability $\alpha = \sum_1^h (1-p)^hp$. The expected time of clustering a read becomes:
$$
  T(|R|) \approx \overline{m}k + m\alpha h + m(1-\alpha)(h + (l-h)\texttt{log}(|G|))
$$

\subsubsection{Aligning}
The complexity of aligning the seeds depend on the size of the windows/clusters. Since dynamic-programming is used, it requires to visit each cell of the matrix once, hence $W_g\times W_r$ where $W_g$ is the size of the window on the genome and $W_r$ is the size of the window on the read. If only one anchor is present in the window, both values are the same and computing the score takes $O(W^2)$ (bounded above by $O(|R|\times W)$ if the window covers the whole read).\par
If several anchors are present, $W$ may be larger than $|R|$ by a number at worse equal to twice the number of anchors times the size of the windows "wings". Backtracking takes a time in $O(|R| + W)$ at worse (considering the worse case as the degenerate "only gaps in both directions" which is not possible since the alignment is local). On average backtracking is in $O(|R|)$.

\subsection{Output examples}
Here are presented some of the alignments produce by the Read-Mapper. The number above represents the position in the read, negated if it comes from the reverse part. The number below is the position in the genome. For the sake of clarity, not all positions are written, but it makes the possible gaps undetectable visually without looking at the read sequences. Dots and double dots denote matches and mismatches.\par
The first sequences are the forward ones and the indented ones are the reverse ones. The vertical lines denote the border of windows.\par

This first example is a map on the forward sequence. As previously stated, considering both the forward and reverse sequence together tends to discard alignments on one of the sequence. This issue is not hard to fix but has not been addressed in the current implementation.\par
\begin{center}
  {\scriptsize\begin{lstlisting}
  HWUSI-EASXXX:7:1:1:45
    |2
  GT|TTACCCAAACGGGTCTGCTGCCGGGTGTGAATTACTTCTTTCTAATTCGAGCCGAGAACTCCCATGGCTTATCA ...
    |::..::.::........:..:..::..::.:..::...:.:::.........:.....:...:...:..::::.
  **|TTTACCCAAACCAAAATCGACGCGGGTTGGACGACAAATCTCTTTAATAGAACCGATCCGTGCTCAGGATATCC ...
    |11159224

            |-5
   ... CTCAG|CTCCACAACATCTCCGGACAGCAGACTGGCACGAGCCTCGCTCAGATCCAGACCACTATTGAAGTAGCGCG
       :::..|:
   ... CTCTT|C**********************************************************************
            |11159304
  \end{lstlisting}}
\end{center}

An example on a reverse sequence.
\begin{center}
  {\scriptsize\begin{lstlisting}
  HWUSI-EASXXX:7:1:0:625

  NCGCCACATCGAGGCTGCTGCTGGGCGGTTTGCTCGACGAACTGATTCGCACCTGGTAGTCCGTCAGTCCCAGTTC ...

  **************************************************************************** ...


       |0                                                                          |-75
   ... |CACCGATGGACGCTACTCGGCGTATGAGCCGCCGTATCTGGGCAACGATGAGTACAACGTTACACTGGCCATTGC|C
       |::::::::::::::::::::::::::::::::.::::::::::::::::::::::::::::::::::::::::::|.
   ... |CACCGATGGACGCTACTCGGCGTATGAGCCGCAGTATCTGGGCAACGATGAGTACAACGTTACACTGGCCATTGC|A
       |18384597                                                                   |18384698
  \end{lstlisting}}
\end{center}

These two next examples exhibit the gain in using both sequences together. An anchor in one sequence can be used to align the other sequence, even if no other anchor have been found.\par
\begin{center}
  {\scriptsize\begin{lstlisting}
  HWUSI-EASXXX:7:1:1:222
    |2
  NG|TCAATCTACATATCAAACTCCTTAGCAGCACATACAGACTCTCTACGATGCGATCTTGGTTTCTCTAGAAGAAG ...
    |::::.:::::.:::::::::::::::::::::::::::::::::::::::..:.::::::.:::::::::::::
  **|TCAACCTACAGATCAAACTCCTTAGCAGCACATACAGACTCTCTACGATGTTAACTTGGTGTCTCTAGAAGAAG ...
    |14746953

                                                    |-45
   ... GTTGCCCTCGCGTCAGTTTGGTTACGTTGTGCTCACCACCTCTGG|CGGCATCATGGACCACGAGGAGGCTAGGAGA
       :..:::.........:..:....:...:.:.:::....:::::.:|.
   ... GAAGCCGAGAATCTTGCCTTCCAAATGTTTTCTCCTAGCCTCTCG|T******************************
                                                    |14747072
  \end{lstlisting}}
\end{center}

\begin{center}
  {\scriptsize\begin{lstlisting}
    HWUSI-EASXXX:7:1:5:1217
             |9
    CAGCATGCT|GCGATGCTCCTGCCACCAGGTCATAATCGACTGCTCCCACTCGTCCTTGGACATCTTGTGCTGGTCG ...
             |.:.....:..:..:..:..:::.:.:.::::....:..:......:.:.:....:.:........::
    *********|TCTGCTTTTTTAGCCACCAGTCCTCAACGACCCTGCGAATCTCGTCCTTGGACACCTTGCGAATGCG ...
             |5587504

                                                                |-55
     ... CATCCGTGCTGCTCGCCTCGTACGCCGTCCAGGCGCGTCATGGTGACCACAATAA|GACCACCCACACAGCCGGCTT
         ..:..::::.:.........:..:::.......:..........:.::..:::::|.
     ... TCTGGGTGCCGTCGATGAGCTCGGCCCGGATCCCAGCGATCCCAGTCCGGAATAA|A********************
                                                                |5587627
  \end{lstlisting}}
\end{center}

This last example is a situation in which no good and long alignment have been found. A better one could probably be made using several windows and yielding a chimeric alignment, but as stated, this functionality has not been implemented.\par
\begin{center}
  {\scriptsize\begin{lstlisting}
  HWUSI-EASXXX:7:1:1:1491

  GCCCCCCCTCCCCTCCCCCCCCCCCCCCCCCACCCCCCCCGCCCACCTCTCCCNCACCCCACCCGCCCGCCCCTCC ...

  **************************************************************************** ...


                         |-19    |-24
  ... CCCCCCCCACACCCCCCCC|ACCCC  |CTCGCCCCCCCCCCCCCCCCCCCCACCANCCGCCACCCCCGCCCTCCACCTC
                         |::::.  |.
  ... *******************|ACCCG  |T***************************************************
                         |3983953|3983958
  \end{lstlisting}}
\end{center}
