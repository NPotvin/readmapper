\documentclass[assignment2_Potvin_Nicolas.tex]{subfiles}

\section{Indexing}
The most useful tool in a Read-Mapper is an index. That is a way to quickly find parts of text based only on "how it begins" thanks to a previous sorting phase using the alphabetical order as a comparison means. The idea here is to index the whole genome, and when one needs to know where a given part of a read can be mapped, a quick look to the index yields all the possible locations in the genome.\par
At first glance, looking in an index sounds a lot like the job of a decision tree, making a decision at each node based on the sequence of characters read from the input word. Suffix trees are inspired from that idea. Due to the tree nature of these indexes, a search can be performed in linear time (in the size of the input word), since at each node a decision is made in constant time based on a character. Even more interesting, they can be built in linear time in the size of the text, which is actually better than the complexity of an efficient sorting algorithm such as quicksort.\par
The issue is the memory needed to store them. repetitions of equal substrings are duplicated in memory, making them huge and not a reasonable solution when dealing with "texts" as large as a genome. A better option would be to reduce memory to the bare minimum, that is a list of positions in the file sorted according to the suffix beginning at that specific position. Considering integers of 32 bits as positions, the size of such an index would be only four times the size of the base text (assuming characters encoded on 8 bits).\par
This type of index is a suffix array. It is basically no more than a sorted list, hence smaller in memory, but slightly slower since a search of a given character is done in logarithmic time in the size of the text.\par
Trees can be represented as arrays in memory if considering potential empty nodes as dummy padding cells. This means that building a suffix array can be done in linear time too, since building a tree, then storing it in memory and finally rewriting it omitting the empty cells are all linear operations. A first approach in building a suffix array would be exactly this. The first studied algorithm however is one that is dedicated suffix arrays especially, namely \textit{Skew}\cite{skew}.

\subsection{Buckets}
Sorting a sequence in linear time is not something especially easy to do and is highly dependent on the type of sequence considered. This speed is gained at the price of memory and this bargain is worth the price only when the sequence is constituted of a lot of elements taking their values in a limited range. In what follows, the set of possible values is called alphabet and the elements are called characters.\par
Instead of comparing characters to one another in order to compute their position in the sorted array, as is usually done, they are themselves considered as indices of an other array of so called buckets. A sorting algorithm called RadixSort or BucketSort uses this idea, working in two steps: first count the number of occurrences of each letters of the alphabet, hence knowing in advance how to partition the sorted resulting array, then associate each character to the right bucket. Then recurse in each bucket and consider the next character of each suffix.\par
\begin{algorithm}[H]
\caption{RadixPass}
\DontPrintSemicolon
    \KwIn{\;
        \Indp
        $W$: A list of words (keys) to sort\;
        $a$: The alphabet size\;
        $i$: the position of the letter to look at when sorting\;
    }
    \KwOut{\;
        \Indp
        $O$: The order in which the words of $w$ are sorted\;
    }
    $B \longleftarrow \{0\}^{a+1}$: the buckets\;
    $O \longleftarrow \{0\}^{|W|}$: the order (the ranks)\;
    \For{$w_i \in W$}{
      $l \longleftarrow w_i$: $l$ is the $i^{th}$ letter of $w$\;
      $B_{l+1} \longleftarrow B_{l+1} + 1$\;
    }
    \For{$l \in \{2, ... a+1\}$}{
      $B_{l} \longleftarrow B_{l} + B_{l-1}$\;
    }
    \For{$j \in \{1, ... |W|\}$}{
      $l \longleftarrow W_{j, i}$: letter $i$ of word $j$\;
      $O_{j} \longleftarrow B_{l}$\;
      $B_{l} \longleftarrow B_{l} + 1$\;
    }
    \Return{$O$}
\end{algorithm}
The complexity of RadixSort cannot be computed according to the number of comparisons since there is no comparisons elements-wise. Nonetheless it may not seem linear at first glance. Counting requires $O(n)$ time, so does the assignment phase. Then the algorithm sorted the suffixes based on only one character, and needs to recurse on each buckets containing more than one element performing a number of operations that seems about the same when considering all the recursive calls on that level, and so on for the whole text.\par
This first thought analysis is biased in the sense that what is interesting in indexing the text is not really sorting the suffixes but quickly find areas of "looking alike" words. Let's say an index of words of length $k$ is to be made, there is no need to sort all possible suffixes, only prefixes of length $k$, which means $k$ recursions that take $O(n)$ time each at worse case hence a total complexity of $O(kn)$ which is indeed linear since $k$ is a constant that should be a lot smaller than $n$ when dealing with large texts.\par
Note: If ever the question arise on how to index longer words, the answer is simply "look again". One first look yields all the positions that match exactly the first $k$ characters of that unusually long word. (And the number of matching positions would probably not be high.) Then the next stop is to look a second time in the index for the remaining part of the word, taking care of considering the matches that begin where the first ones stopped.\par
One last remark needs to be made concerning the order in which RadixSort operates. There are two versions, one dealing first with the most significant character of the words (MSB) then the second most significant and so on; this is the version presented previously. And there is a version going backward and sorting the words beginning by their least significant characters first (LSB).\par
There are good arguments for both in terms of performances, for the LSB, the expected complexity is lower when considering words of variable length (since it finishes sorting shorter keys faster than MSB), but that case is not relevant in this case. More practically, LSB tends to move around larger and larger groups of words, which can lower the number of cache misses when dealing with a large number of words. On the opposite, MSB tends to make smaller and smaller "strides" when moving words around since they are always allocated to a narrower and narrower area until they fit completely in a single bucket. This is particularly useful in a divide and conquer approach, since all large buckets previously dealt with are guaranteed to not need any sorting between them any more.

\subsection{Skew}
The previous section introduced RadixSort, which is extensively used in this work. The first reason is because it is a sub-routine of Skew\cite{skew}. The idea behind Skew is to build a suffix array in linear time "for real". The used approach consider the text as being made of three sub-texts being written in turns \ie the first character of the first text is written, then the first of the second text, then the first of the third and only then the second of the first text, the second of the second ... These sub-texts are named $S0$, $S1$ and $S2$ where the number corresponds to $i\ (mod)\ 3$ with $i$ the indices of their respective characters.\par
One observation to be made is that the final position of a word in the index depends its first character, and then on all those following. This sounds silly but is actually a powerful idea since only two elements are considered here, one character and a so called "rank" attributed to the remaining suffix. In the three sub-texts considered previously, this would mean that the positions of the elements of $S0$ in the whole text would depend on their relative order (based on only one character) and mostly on the order in which the elements of the two others sub-texts have been sorted (since they constitute the suffix of each element of $S0$).\par
The good news is that first, there is already a sorting algorithm working in linear time (especially for words one-letter long) and second, ranking the $S1$ and $S2$ sub-texts is nothing more than another sort of suffixes, hence a perfect task for a recursive call to Skew.\par
The genius idea is to call Skew not only on a two-third of the original text at each call but also to present to lower-level calls a text that is a sort of an information-related denser text as input, made of the "name of the bucket" each element belong to. Which is effectively a way to order them (\ie the elements from bucket A come before those from bucket B). The next call has to deal with the repetitions in each bucket \textit{if any}. This is the stop condition that occurs fast since at each layer the alphabet may grow, (hence the number of buckets). The growth of the alphabet may slow Radix, but it is important to note that the more there are repetitions in the buckets, the less buckets are needed (since some are empty), hence the smaller is the alphabet necessary to differentiate them.\par
After the call to Skew, the elements of $S1$ and $S2$ are guaranteed to have a unique "name" (or rank), which is all that was needed to sort the elements of $S0$ (since their suffixes are already sorted). Applying a merging phase, the two parts $S1-2$ and $S0$ are used to sort the original text. A C++ implementation of the code is given in \cite{skew} and has been adapted to python in this project (taking the shortcuts made possible by numpy's array programming). To lower the computational cost of a full python implementation, Numba is used to compile the code \textit{Just In Time}.\par
The algorithm is not reproduced here as it is well explained in \cite{skew}. In terms of complexity, Radix sort passes are linear and used a constant number of time, the merging process is linear too, and the recursive call work on a text of size $2/3$ of the original text, hence the total time $T(n)$ is $T(n) = T(2n/3) + O(n)$. As proved in the skew article\cite{skew}, $T(n) = O(n)$ since the limit case $n < 3$ is solved in constant time, by induction $T(n)$ is linear for any $n$.\par
(Note: Numba is a very easy to use library allowing the compilation of pure-python code to LLVM and running an optimised version at runtime. For heavy tasks, the time lost in compilation is largely worth the gain in computational speed, but sadly Numba does not like cython nor user-defined python classes, which makes it more a wild-card than a swiss-knife.)

\subsection{Parallel sort}
Skew is a powerful tool but it clearly adopts an LSB strategy (suffixes are sorted first). Which makes the sorting of the first call dependent on the results of all the following calls. This may be fast on the paper, but in the silicon, there is still only one process doing all the work. This issue is addressed by Rajasekaran and Nicolae \cite{RAJASEKARAN201421}. Their work aims at effectively boosting the performance of suffix array construction. Three algorithms are presented that are a logical succession of each other. The two firsts were the inspiration for the algorithm used in this work. The third would have been the one used in the final implementation of this work if time would not have been an issue.\par
The motivation behind the use of Rajasekaran's algorithm is, on a development level, its ease of implementation once Skew and RadixSort are already implemented, on a performance level, the unlocking of full parallelisation of the indexing, which boosts the actual computational speed and from a theoretical point of view a higher probability to reach actual linear time on any input is worth mentioning (but this was not an issue in this case since not achieving linear time with a fixed-size alphabet of 4-5 letters using Radix on fixed-size keys is rather improbable).\par
Apart from the algorithm and the parallelisation in itself, Rajasekaran's work is filled with observations allowing a significant speed-up of the base algorithms themselves. As an example, instead of counting/assigning/recurse in radix, using the assignment phase of a call to already count the buckets' size of the next, reducing the number of passes needed on the whole text. Or going even further, merge successive Radix calls together by considering buckets as containing combinations of letters rather than only one, which fundamentally reduces to consider an alphabet of couples/triples/quadruples rather than singletons.\par
The gain is time (fewer passes), the price is memory once again, but moving from an alphabet of 5 characters to one of 25, or 125 or even more is almost negligible for a modern computer. This is even more precious when considering that due to its size, the suffix array is sorted in place rather than copied at each pass, and each swap operation could potentially result in a cache miss and a need of (slow) disk I/O operation.\par

\subsection{Implementation}
The theory behind the current implementation has been covered, and some technical concerns have already been addressed. On a practical matter, the proposed implementation makes use of cython to interface two C++ classes using file operations at a lower level than the one of python and allowing the allocation of potentially big memory caches. This cache management has been crucial in the speed-up of the suffix array construction since they allowed to cache the entire genome and suffix arrays in the memory while building it. Indexing the chromosome 2L of the fruit fly has been done in less than 15 minutes on 6 parallel processes including a full check of the sorted array. On the whole genome, less than an hour was necessary (not including the check, this time). In both cases index entries are 70 characters long since most reads seem to be about that size.\par
The construction occurs in three phases. First a "multi-pass" of Radix is used (\ie Radix using buckets of combinations of letters as explained previously). This phase is necessarily single-threaded and has the purpose of splitting the work between parallel workers. It is important to note that the initial single file of unsorted indices is split in several smaller ones after this phase. This is done in order to make possible a disk parallelisation (not possible on the used machine for hardware reasons and made useless by the use of large cache.)\par
The second phase is already multi-threaded and further the possibility to split the files in independent sub-files. This again yields no real gain on the used machine and was thought of a useful feature when working on larger files stored on parallel disks that do not hold whole in the memory. Parallelisation at a disk level can highly speed-up the construction when cache misses occur often since access to a disk is sequential, the only way to parallelise such operations is to use different files placed on physically different disks.\par
While the two first phases implemented the pre-sorting phase of the first algorithm presented in \cite{RAJASEKARAN201421}, the third phase consists in using Skew to finish the job. Efficient use of the vectorisation of numpy enhanced by numba's JIT were the key tools at that level. Integer encoded on 32 bits are used in the arrays (and almost everywhere else in this project) in order to facilitate both the python interpreter's vectorisation job and the use of intel CPU's hyper-threading technology.\par
(Note: in STAR there is a mention of using 33bits integer for really big genomes. This possibility has not been considered in this work.)

\subsection{Pre-Indexing}
The pre-indexing strategy used in this work follows an idea present in the supplementary response of STAR\cite{star}. It is meant to speed-up searching in the index. The pre-index is built on data that are written on files during the building of the suffix array. These data consist in records on the positions where entries prefix of a given length shift from one to another. It is stored in memory during the mapping and follows a tree-like structure, allowing either linear time search of a word being small enough or to narrow greatly all searches in the suffix arrays before even consulting the actual index. This strategy has been reported as speeding up STAR's alignments by a factor 2-4.\par
In this work, the pre-index depth is 12. It is organised as a tree encoded as a numpy array where all nodes have 5 children. Access to the children of any node $i$ is achieved by looking in the array at indices $j+1, j+2, j+3, j+4, j+5$ where $j = i\times 5$. The root of the tree (\ie the node at index $i = 0$) represents the empty word, which is technically not present in the genome, but for implementation reasons, it is present in the tree (otherwise the children indices' computation becomes a real nightmare).\par
The depth of the pre-index depends on the amount of memory one is willing to allocate to such a tool. 12 as a depth with an alphabet of 5 letters using 32bits integers translates to an array of $4*5^{12+1}-1 \approx 4.9Gb$ which has been considered small enough for the speed-up it provides.


















%
