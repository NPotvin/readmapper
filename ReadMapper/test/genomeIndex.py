import numpy as np
from random import shuffle
from DataStructures.GenomeIndex import GenomeIndex
from cppModules.Suffix.SuffixX import Suffix
from cppModules.SuffixArray.SuffixArrayX import SuffixArray
from utils.fileHandling import setupGenomeFiles, recoverSlices
from algorithms.genomeIndexing import indexGenome
from utils.checkSort import checkSort


def testIndexing():
    chrIndexFilename, genomeFilename, arraySlicesFilebase = \
        setupGenomeFiles("data/test.fa", "mapData/test/Index/", override=True)
    prefLen = 70
    preIndLen = 12
    suffix = Suffix(genomeFilename, cacheSize=1)
    sa = SuffixArray(arraySlicesFilebase, 0, suffix.size, init=True)
    indexGenome(suffix, prefixLen=prefLen, preIndPrefLen=preIndLen, sa=sa, recursDepth=2, sliceDepth=3, maxThreads=4)

    slices = recoverSlices(arraySlicesFilebase)
    ends = np.array([end for _, _, end, _, _, _ in slices], dtype=np.int32)
    checkSort(suffix, arraySlicesFilebase, ends, 8)

    gIndex = GenomeIndex(preIndLen, prefLen, "mapData/test/Index")
    indices = np.arange(suffix.size, dtype=np.int32)
    shuffle(indices)
    for i in indices:
        word = suffix.getWord(i, prefLen)
        ids, matchLen = gIndex.index(word, prefLen)
        assert ids.size > 0  # word in index
        found = False
        for id in ids:
            loc = gIndex.location(id)
            found = found or loc == i
            assert np.array_equal(gIndex.wordFromIndex(id, prefLen), word)  # consistent suffix read
            assert np.array_equal(gIndex.word(loc, prefLen), word)  # consistent suffix read
            assert np.array_equal(suffix.getWord(loc, prefLen), word)  # consistent suffix read
        assert found  # specific suffix in index

