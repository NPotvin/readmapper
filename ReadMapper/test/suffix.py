from utils.Base import Base
from cppModules.Suffix.SuffixX import Suffix
from cppModules.Suffix.Suffix160 import Suffix as Suffix160
from utils.fileHandling import setupGenomeFiles


def testCSuffix():
    chrIndexFilename, genomeFilename, _ = setupGenomeFiles(
        "data/test.fa", dest="mapData/test/Index/", override=False)
    array = Suffix(genomeFilename, cacheSize=10)
    print(genomeFilename)
    print(array.size)
    print("".join(map(lambda b: Base(b).name if b>0 else 'X', array.getWord(0, 10))))
    print()
    print("".join(map(lambda b: Base(b).name if b>0 else 'X', array.getWord(45, 10))))
    print()
    print("".join(map(lambda b: Base(b).name if b>0 else 'X', array.getWord(0, 101))))
    print()
    print("".join(map(lambda b: Base(b).name if b>0 else 'X', array.getWord(50, 51))))
    print()
    print("".join(map(lambda b: Base(b).name if b>0 else 'X', array.getWord(51, 50))))
    print()
    print("".join(map(lambda b: Base(b).name if b>0 else 'X', array.getWord(51, 49))))
    print()
    print("".join(map(lambda b: Base(b).name if b>0 else 'X', array.getWord(array.size-5, 10))))
    print()


def testCSuffix16(letters=False):
    chrIndexFilename, genomeFilename, _ = setupGenomeFiles(
        "data/test.fa", dest="mapData/test/Index/", override=False)
    array = Suffix160(genomeFilename)
    print(genomeFilename)
    print(array.size)
    toPrint = lambda b: Base(b).name if b>0 else 'X' if letters else str(b)
    print("".join(map(toPrint, array.getWord(0, 10))))
    print()
    print("".join(map(toPrint, array.getWord(0, 5))))
    print()
    print("".join(map(toPrint, array.getWord(5, 10))))
    print()
    print("".join(map(toPrint, array.getWord(15, 1))))
    print()
    print("".join(map(toPrint, array.getWord(0, 16))))
    print()
    print("".join(map(toPrint, array.getWord(45, 10))))
    print()
    try:
        print("".join(map(toPrint, array.getWord(0, 101))))
    except IndexError as e:
        print("caught expected error :", e)
    print()
    print("".join(map(toPrint, array.getWord(array.size-5, 10))))
    print()
    print(toPrint(array.get(array.size-5)))
    print()
    print(toPrint(array.get(5)))
    print()