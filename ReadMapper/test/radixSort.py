from utils.checkSort import checkSort
from algorithms.radixSort import twoStepsRadixSort
from algorithms.radixSortStudy import radixSort, hybridRadixSort, fourStepsRadixSort
from cppModules.Suffix.SuffixX import Suffix
from cppModules.SuffixArray.SuffixArrayX import SuffixArray
from utils.fileHandling import setupGenomeFiles



def testRadixSort():
    chrIndexFilename, genomeFilename, arraySlicesFilebase = setupGenomeFiles(
        "data/test.fa", dest="mapData/test/Index/", override=True)
    arraySlicesFilebase = "mapData/test/Index/slices/suffixArray"
    suffix = Suffix(genomeFilename, cacheSize=1)
    sa = SuffixArray(arraySlicesFilebase, 0, suffix.size, init=True, cacheSize=1)
    prefLength = 8
    ends, swapNum = radixSort(suffix, prefLength, sa)
    checkSort(suffix, arraySlicesFilebase, ends, prefLength)


def testRadixSortTwoSteps():
    chrIndexFilename, genomeFilename, arraySlicesFilebase = setupGenomeFiles(
        "data/test.fa", dest="mapData/test/Index/", override=True)
    arraySlicesFilebase = "mapData/test/Index/slices/suffixArray"
    suffix = Suffix(genomeFilename, cacheSize=1)
    sa = SuffixArray(arraySlicesFilebase, 0, suffix.size, init=True, cacheSize=1)
    prefLength = 8
    ends, swapNum = twoStepsRadixSort(suffix, prefLength, sa, slice=True)
    checkSort(suffix, arraySlicesFilebase, ends, prefLength)


def testHybridRadixSort():
    chrIndexFilename, genomeFilename, arraySlicesFilebase = setupGenomeFiles(
        "data/test.fa", dest="mapData/test/Index/", override=True)
    arraySlicesFilebase = "mapData/test/Index/slices/suffixArray"
    suffix = Suffix(genomeFilename, cacheSize=1)
    sa = SuffixArray(arraySlicesFilebase, 0, suffix.size, init=True, cacheSize=1)
    prefLength = 8
    ends, swapNum = hybridRadixSort(suffix, prefLength, sa, recursDepth=1)
    checkSort(suffix, arraySlicesFilebase, ends, prefLength)


def testFourStepsRadixSort():
    chrIndexFilename, genomeFilename, arraySlicesFilebase = setupGenomeFiles(
        "data/test.fa", dest="mapData/test/Index/", override=True)
    suffix = Suffix(genomeFilename, cacheSize=1)
    sa = SuffixArray(arraySlicesFilebase, 0, suffix.size, init=True, cacheSize=1)
    prefLength = 8
    ends, swapNum = fourStepsRadixSort(suffix, prefLength, sa, recursDepth=1)
    checkSort(suffix, arraySlicesFilebase, ends, prefLength)





