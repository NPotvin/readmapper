import re
import numpy as np
from algorithms.skew import skew


def testSkew():
    loremipsum = r"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare ante maximus elit egestas varius. Cras lobortis urna non orci consectetur gravida. Vivamus sed est non urna semper faucibus. Donec sollicitudin purus at quam ultrices maximus. Nullam convallis euismod enim, in tincidunt felis facilisis posuere. Quisque a massa imperdiet, vulputate lectus eu, fermentum neque. Sed augue ligula, convallis id consectetur nec, dignissim at eros. Integer vel erat vel ipsum molestie laoreet. Mauris congue leo dolor, ut consequat ligula viverra id. Phasellus congue eros dui, eu tempus ex porta ac. Maecenas eget luctus leo. Ut a dignissim ante. Donec vitae tellus in enim molestie ullamcorper et id massa. Pellentesque placerat convallis dictum. Pellentesque tincidunt facilisis consectetur. Phasellus molestie tempus odio in vulputate." \
                 r"Vivamus et vestibulum lorem. Ut aliquet malesuada pulvinar. Phasellus et ante commodo, efficitur tellus sit amet, euismod nulla. Quisque vel quam rhoncus, lacinia elit nec, porttitor quam. Nunc elementum dictum facilisis. Aenean mi tortor, mollis suscipit mauris faucibus, fringilla vulputate ex. Cras quis nisl ac erat lobortis rhoncus sit amet quis nulla. Cras ornare consequat erat, id dapibus dolor convallis et. Maecenas in sapien id massa efficitur congue. Mauris semper ex nibh, ut eleifend neque ornare vel. Phasellus pellentesque gravida arcu ac pretium. Nam volutpat, felis aliquet gravida blandit, augue odio feugiat lorem, sit amet cursus massa tellus vitae mauris. Integer condimentum eros est, ac consectetur ipsum euismod eget. Nulla fringilla quam id feugiat dictum." \
                 r"Aenean sodales molestie libero, at commodo velit efficitur eu. Quisque sollicitudin sed ex sed consectetur. Morbi pellentesque quam varius dapibus placerat. Cras felis erat, feugiat eu dignissim at, pharetra non urna. Integer pretium orci eget ligula suscipit, a suscipit massa commodo. Suspendisse viverra egestas mauris ac scelerisque. Nulla aliquam a ex at placerat. Donec a turpis id leo feugiat auctor. Integer sit amet suscipit orci." \
                 r"Quisque vestibulum leo ut tempor placerat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed tempor semper magna ut pharetra. Vivamus et vehicula justo. Ut eu ipsum facilisis, commodo ante a, tempus lacus. Nam sit amet efficitur odio. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum fringilla, justo sed feugiat euismod, urna ex porttitor augue, id faucibus sapien urna quis risus. Ut in ligula aliquam, vestibulum tortor nec, iaculis tellus." \
                 r"Mauris sit amet sapien sed libero malesuada venenatis eu sed risus. Sed egestas ornare neque, ac ornare eros auctor laoreet. Cras ac tortor finibus, laoreet metus vitae, suscipit nisl. Nunc risus felis, mollis ac varius quis, ullamcorper sit amet massa. Proin sodales nisl id nunc vehicula volutpat. Donec feugiat laoreet nisl, auctor vestibulum massa dignissim vel. In venenatis est eget elit viverra eleifend. Integer porttitor venenatis iaculis."
    loremipsum = re.sub("[,.]", "", loremipsum.lower())
    liArray = np.array([ord(l) for l in loremipsum], dtype=np.int32)

    length = liArray.size - liArray.size % 3 - 3
    liArray[length - 3:] = 0
    loremipsum = loremipsum[:length - 3] + 'X' * (len(loremipsum) - length)

    sa = np.empty(length, dtype=np.int32)
    skew(liArray, sa, length, max(liArray))
    sa = sa[3:]  # the first three are "XXX..."
    prev = loremipsum[sa[0]:sa[0] + 5]
    for i in range(sa.size):
        #print(loremipsum[sa[i]:sa[i] + 5])
        assert prev <= loremipsum[sa[i]:sa[i] + 5]
        prev = loremipsum[sa[i]:sa[i] + 5]
