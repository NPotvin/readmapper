import numpy as np
from utils.Base import Base


class Read:

    def __init__(self, readId, forwardSequence, reverseSequence, forwardQuality, reverseQuality):
        self._id = readId
        self._sequence = np.array([forwardSequence, reverseSequence], dtype=np.int32)
        self._quality = np.array([forwardQuality, reverseQuality], dtype=np.int32)

    @property
    def sequence(self): return self._sequence
    @property
    def fullSequence(self): return self._sequence.flatten()
    @property
    def quality(self): return self._quality

    @property
    def forwardSequence(self): return self._sequence[0]
    @property
    def reverseSequence(self): return self._sequence[1]
    @property
    def forwardQuality(self): return self._quality[0]
    @property
    def reverseQuality(self): return self._quality[1]

    @property
    def readId(self): return self._id
    @property
    def size(self): return self._sequence.shape[1]

    def __str__(self):
        res = f"{self.readId}#0/1\n" \
              f"{''.join(Base(b).name for b in self.forwardSequence)}\n" \
              f"+{self.readId}#0/1\n" \
              f"{''.join(chr(q) for q in self.forwardQuality)}\n" \
              f"{self.readId}#0/2\n" \
              f"{''.join(Base(b).name for b in self.reverseSequence)}\n" \
              f"+{self.readId}#0/2\n" \
              f"{''.join(chr(q) for q in self.reverseQuality)}"
        return res


