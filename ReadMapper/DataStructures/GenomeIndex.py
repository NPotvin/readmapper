import os
from cppModules.Suffix.SuffixX import Suffix
from cppModules.Suffix.Suffix80 import Suffix as Suffix80
from cppModules.Suffix.Suffix160 import Suffix as Suffix160
from cppModules.SuffixArray.SuffixArrayX import SuffixArray
import numpy as np
from utils.Base import Base
from utils.fileHandling import loadSA, recoverPreindex, recoverChrIndices


class GenomeIndex:

    def __init__(self, preIndexDepth, prefixMaxLength, arrayDataPath, cache=160,
                 chrIndexFilename="chromosomeIndices.txt", genomeFilename="genome.txt",
                 arraySlicesFilebase=os.path.join("slices", "suffixArray")):
        if cache <= 80: self._suffix = Suffix80(os.path.join(arrayDataPath, genomeFilename))
        elif cache <= 160: self._suffix = Suffix160(os.path.join(arrayDataPath, genomeFilename))
        else: self._suffix = Suffix(os.path.join(arrayDataPath, genomeFilename), cacheSize=cache*10**6)
        self._sa = SuffixArray(*loadSA(os.path.join(arrayDataPath, arraySlicesFilebase), cacheSize=cache*10**6))
        self._chrIndex = recoverChrIndices(os.path.join(arrayDataPath, chrIndexFilename))
        self._preIndexDepth = preIndexDepth
        self._prefixMaxLength = prefixMaxLength
        self._preIndex = \
            GenomeIndex.buildPreIndex(preIndexDepth, self._suffix, arrayDataPath) if preIndexDepth else None

    @staticmethod
    def buildPreIndex(preIndexDepth, suffix, arrayDataPath):
        preindexDumpFile = os.path.join(arrayDataPath, "preIndex.npy")
        if os.path.exists(preindexDumpFile):  # if a previous run built it, load and return
            return np.load(preindexDumpFile)
        preIndex = np.full(len(Base)**(preIndexDepth+1)-1, fill_value=-1, dtype=np.int32)
        for saId, suffId in recoverPreindex(arrayDataPath):
            word = suffix.getWord(suffId, preIndexDepth) - 1
            i, j = 0, 0
            while i < preIndexDepth and word[i] != -1:
                j = j*len(Base) + word[i] + 1
                if preIndex[j] == -1 or preIndex[j] > saId:
                    preIndex[j] = saId
                i += 1
        np.save(preindexDumpFile, preIndex)
        return preIndex

    @property
    def size(self):
        return self._sa.size

    def wordFromIndex(self, index, length):
        return self._suffix.getWord(self._sa[index], length)

    def word(self, location, length):
        return self._suffix.getWord(location, length)

    def location(self, index):
        return self._sa[index]

    def preIndex(self, word, length):
        length = min(self._preIndexDepth, length)
        i, start, end = 1, 0, self._sa.size
        j = word[0]
        while i < length and self._preIndex[j] != -1 and word[i] > 0:
            j = j*len(Base) + word[i]
            i += 1
        if self._preIndex[j] != -1:
            start = self._preIndex[j]
        else:
            start = self._preIndex[(j-1)//len(Base)]
        matchLen = i
        # there might be a nearer end than the last entry in sa. Need to look around
        while i > 0:  # going back up the tree until an end is found
            k = 0
            while (j+k)%len(Base):
                k += 1
                if self._preIndex[j+k] > start:
                    return start, self._preIndex[j+k], matchLen
            j = (j-1)//len(Base)
            i -= 1
        return start, end, matchLen

    def index(self, word, length):
        start, end, offset = self.preIndex(word, length)
        if self._preIndexDepth and offset == 0: return np.array([], dtype=np.int32), 0
        if end-start == 1 or offset == length:
            return np.arange(start, end, dtype=np.int32), offset
        letter = lambda saId, off: self._suffix.get(self._sa[saId] + off)
        for i in range(offset, length):
            if letter(start, i) > word[i] or letter(end-1, i) < word[i]:  # no further match can be found
                return np.arange(start, end, dtype=np.int32), i
            if letter(start, i) < word[i]:  # we need to find the new start
                j = start + (end-start)//2
                beforeStart, afterStart = start, end
                while afterStart-beforeStart > 1 and j > 0 and (letter(j-1, i) == word[i] or letter(j, i) != word[i]):
                    if letter(j, i) >= word[i]: afterStart = j
                    else: beforeStart = j
                    j = beforeStart + (afterStart-beforeStart)//2
                start = j
            if letter(end-1, i) > word[i]:  # we need to find the new end
                j = start + (end-start)//2
                beforeEnd, afterEnd = start, end
                while afterEnd-beforeEnd > 1 and j > 0 and (letter(j-1, i) != word[i] or letter(j, i) == word[i]):
                    if letter(j, i) > word[i]: afterEnd = j
                    else: beforeEnd = j
                    j = beforeEnd + (afterEnd-beforeEnd)//2
                end = j
        return np.arange(start, end, dtype=np.int32), length

    def locate(self, word, length):
        indices, length = self.index(word, length)
        return np.array(list(map(self.location, indices)), dtype=np.int32), length




















