#include <iostream>

#include "Suffix/Suffix.hpp"
#include "SuffixArray/SuffixArray.hpp"


void testSuffix() {
    Suffix<16> suffix(
            "/home/lethalpizza/Cours/AdvancedBioinfo/assignments/assignment2-ReadMapping/ReadMapper/mapData/test/suffArrays/genome.txt",
            16);

    auto res(suffix.get(0, 10));
    std::cout<<suffix.size()<<std::endl;
    for (int i(0); i<10; ++i) std::cout<<(int)res[i];
    std::cout<<std::endl;

    res = suffix.get(0, 5);
    for (int i(0); i<5; ++i) std::cout<<(int)res[i];
    std::cout<<std::endl;

    res = suffix.get(5, 10);
    for (int i(0); i<10; ++i) std::cout<<(int)res[i];
    std::cout<<std::endl;

    res = suffix.get(15, 1);
    std::cout<<(int)res[0];
    std::cout<<std::endl;

    res = suffix.get(0, 16);
    for (int i(0); i<16; ++i) std::cout<<(int)res[i];
    std::cout<<std::endl;

    res = suffix.get(45, 10);
    for (int i(0); i<10; ++i) std::cout<<(int)res[i];
    std::cout<<std::endl;

    res = suffix.get(suffix.size()-5, 10);
    for (int i(0); i<10; ++i) std::cout<<(int)res[i];
    std::cout<<std::endl;

    res = suffix.get(suffix.size()-5);
    std::cout<<(int)res[0];
    std::cout<<std::endl;

    res = suffix.get(5);
    std::cout<<(int)res[0];
    std::cout<<std::endl;

    res = suffix.get(4);
    std::cout<<(int)res[0];
    std::cout<<std::endl;

    res = suffix.get(3);
    std::cout<<(int)res[0];
    std::cout<<std::endl;

    res = suffix.get(0, 7);
    for (int i(0); i<7; ++i) std::cout<<(int)res[i];
    std::cout<<std::endl;

    res = suffix.get(5);
    std::cout<<(int)res[0];
    std::cout<<std::endl;

    res = suffix.get(4);
    std::cout<<(int)res[0];
    std::cout<<std::endl;

    res = suffix.get(3);
    std::cout<<(int)res[0];
    std::cout<<std::endl;
}


int main() {
    //std::string filename("/home/lethalpizza/Cours/AdvancedBioinfo/assignments/assignment2-ReadMapping/ReadMapper/cppModules/SuffixArray/testSA");
    std::string filename("/home/lethalpizza/Cours/AdvancedBioinfo/assignments/assignment2-ReadMapping/ReadMapper/mapData/test/suffArrays/suffixArray");
    SuffixArray<> sa(filename, 0, 2730, 16, false);
    sa.open(false);

    for (long i(0); i < 85; ++i)
        std::cout<<(int)sa.get(i)<<" "<<std::flush;
    std::cout<<std::endl;

    /*
    sa.put(1, 0);
    sa.put(0, 1);
    sa.get(15);
    sa.put(2, 20);
    sa.get(19);
    sa.put(3, 21);
    sa.put(19, 22);
    sa.get(0);


    for (int i(0); i < sa.size(); ++i)
        std::cout<<(int)sa.get(i)<<" ";
    std::cout<<std::endl;
     */
}

