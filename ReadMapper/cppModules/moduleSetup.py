import sys

from setuptools import setup, Extension
from Cython.Build import cythonize
from scipy._build_utils import numpy_nodepr_api
import numpy as np
from utils.utils import createDir, cleanDir
from os.path import dirname, realpath, join, exists
from os import rename


def produceExtension(moduleName, specialisationDir, needsCython, debug):
    thisDir = dirname(realpath(__file__))
    moduleDir = join(thisDir, moduleName)
    compiledDir = createDir(join(specialisationDir, "compiled"))
    extraArgs = ["-std=c++17", "-march=native", "-O0" if debug else "-O3"]
    if needsCython:
        source = join(moduleDir, f"{moduleName}.pyx")
    else:
        source = join(specialisationDir, f"{moduleName}.cpp")
    extension = Extension(
        moduleName,
        sources=[source],
        include_dirs=[
            join(thisDir, "eigen"),
            moduleDir,
            specialisationDir,
            compiledDir,
            np.get_include()
        ],
        language="c++",
        extra_compile_args=extraArgs,
        extra_link_args=extraArgs,
        library_dirs=["libcpp"],
        **numpy_nodepr_api
    )
    if needsCython:
        return cythonize([extension], compiler_directives={"language_level": "3"})
    return [extension]


def compileModule(moduleName, specialisation, needsCython, debug):
    moduleDir = join(dirname(realpath(__file__)), moduleName)
    specialisationDir = join(moduleDir, specialisation)
    compiledDir = join(specialisationDir, "compiled")
    if needsCython:
        cleanDir(compiledDir)
    extension = produceExtension(moduleName, specialisationDir, needsCython, debug)
    save = sys.argv
    sys.argv = [save[0], "build_ext"]
    setup(
        ext_modules=extension,
        options={"build": {"build_lib": compiledDir}},
        zip_safe=False,
        install_requires=['numpy'],
    )
    sys.argv = save
    if needsCython and exists(join(moduleDir, f"{moduleName}.cpp")):
        cythonizedSource = join(specialisationDir, f"{moduleName}.cpp")
        rename(join(moduleDir, f"{moduleName}.cpp"), cythonizedSource)


def setupModules(override=False, debug=False):
    compileModule("Suffix", "SuffixX", override, debug)
    compileModule("Suffix", "Suffix160", override, debug)
    compileModule("Suffix", "Suffix80", override, debug)
    compileModule("SuffixArray", "SuffixArrayX", override, debug)
