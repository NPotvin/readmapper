#ifndef CPPMODULES_SUFFIXARRAY_HPP
#define CPPMODULES_SUFFIXARRAY_HPP


#include <Eigen/Eigen>
#include <iostream>
#include <fstream>
#include <algorithm>


template <Eigen::Index CacheSize=Eigen::Dynamic>
class SuffixArray {
private:

    using Cache = Eigen::Vector<std::streamoff, CacheSize==0 ? Eigen::Dynamic : CacheSize>;
    Eigen::Index _start;
    Eigen::Index _end;
    Cache _cache;
    std::streamoff _cachedFirstId;
    Eigen::Index _cacheSize;
    std::string _basename;
    std::string _file;
    bool _readOnly;
    bool _dirty;

    [[nodiscard]] constexpr Eigen::Index buffSize() { return _cacheSize*sizeof(std::streamoff); }
    [[nodiscard]] inline std::string getFilename(const Eigen::Index& start, const Eigen::Index& end) const {
        return _basename + std::to_string(start) + '_' + std::to_string(end) + ".txt";
    }

    inline void initializeFile() {
        std::ofstream file(_file, std::ios::binary);
        auto offset((_end - _start) % _cacheSize);
        std::streamoff pos(_start);
        for (; pos < _end - offset;) {
            for (Eigen::Index i(0); i < _cacheSize; ++i)
                _cache[i] = pos++;
            file.write(reinterpret_cast<char*>(&_cache[0]), buffSize());
        }
        for (Eigen::Index i(0); i < offset; ++i) _cache[i] = pos++;
        file.write(reinterpret_cast<char*>(&_cache[0]), offset*sizeof(std::streampos));
        file.close();
    }

    template<bool Readonly=true>
    inline std::fstream open(const std::streamoff& index) {
        std::fstream file;
        if constexpr (Readonly) file.open(_file, std::ios::in | std::ios::binary);
        else file.open(_file, std::ios::in | std::ios::out | std::ios::binary);
        file.seekg((index - _start)*sizeof(std::streamoff));
        return file;
    }

    constexpr void flush() {
        if (!_readOnly && _dirty) {
            auto file(open<false>(_cachedFirstId));
            file.write(reinterpret_cast<char*>(&_cache[0]), buffSize());
            file.close();
        }
    }

    constexpr Eigen::Index load(const Eigen::Index& index) {
        flush();  // this does nothing if in read-only
        _cachedFirstId = std::min(index, _end - _cacheSize);
        auto file(open(_cachedFirstId));
        file.read(reinterpret_cast<char*>(&_cache[0]), buffSize());
        file.close();
        return index - _cachedFirstId;
    }

public:

    inline SuffixArray() : _start(0), _end(0), _cache(), _cachedFirstId(0), _cacheSize(0), _basename(), _file(),
                           _readOnly(true), _dirty(false) {}
    ~SuffixArray() { flush(); }

    inline SuffixArray(std::string basename, const Eigen::Index& start, const Eigen::Index& end,
        const Eigen::Index& cacheSize, const bool& readOnly) :
            _start(start), _end(end), _cache(CacheSize == Eigen::Dynamic ? std::min(_end-_start, cacheSize) : CacheSize),
            _cachedFirstId(start), _cacheSize(std::min(_end-_start, _cache.rows())), _basename(std::move(basename)),
            _file(getFilename()), _readOnly(readOnly), _dirty(false) {}

    inline SuffixArray(const SuffixArray& other) : _start(other._start), _end(other._end), _cache(Cache(other._cache)),
       _cachedFirstId(other._cachedFirstId), _cacheSize(other._cacheSize), _basename(other._basename),
       _file(getFilename()), _readOnly(other._readOnly), _dirty(false) { other.flush(); }
    inline SuffixArray(SuffixArray&& other)  noexcept : _start(std::move(other._start)), _end(std::move(other._end)),
        _cache(std::move(other._cache)), _cachedFirstId(std::move(other._cachedFirstId)),
        _cacheSize(std::move(other._cacheSize)), _basename(std::move(other._basename)),
        _file(getFilename()), _readOnly(std::move(other._readOnly)), _dirty(false) {}

    constexpr void open(const bool& initialize) {
        if (_readOnly && initialize)
            throw std::logic_error("SuffixArray::SuffixArray(...) cannot initialize a file in read-only mode");
        if (initialize) initializeFile();
        auto file(open(_start));
        file.read(reinterpret_cast<char*>(&_cache[0]), buffSize());
        file.close();
        _cachedFirstId = _start;
    }

    constexpr SuffixArray& operator=(const SuffixArray& other) {
        other.flush();
        _start = other._start;
        _end = other._end;
        _cache = Cache(other._cache);
        _cachedFirstId = other._cachedFirstId;
        _cacheSize = other._cacheSize;
        _basename = other._basename;
        _file = other._file;
        _readOnly = other._readOnly;
        _dirty = false;
        return *this;
    }
    constexpr SuffixArray& operator=(SuffixArray&& other) noexcept {
        _start = std::move(other._start);
        _end = std::move(other._end);
        _cache = std::move(other._cache);
        _cachedFirstId = std::move(other._cachedFirstId);
        _cacheSize = std::move(other._cacheSize);
        _basename = std::move(other._basename);
        _file = std::move(other._file);
        _readOnly = std::move(other._readOnly);
        _dirty = std::move(other._dirty);
        return *this;
    }

    constexpr const std::streamoff& get(const Eigen::Index& index) {
        if (index < _start || index >= _end) throw std::out_of_range(
                    "SuffixArray::put(const Eigen::Index&, const std::streamoff&) index "+
                    std::to_string(index)+" out of range ["+std::to_string(_start)+", "+std::to_string(_end)+")");
        auto cacheId(index - _cachedFirstId);
        if (index < _cachedFirstId || index >= _cachedFirstId + _cacheSize)  // cache miss
            cacheId = load(index);
        return _cache[cacheId];
    }

    constexpr void put(const Eigen::Index& index, const std::streamoff& suffixId) {
        if (_readOnly) throw std::runtime_error(
                "SuffixArray::put(const Eigen::Index&, const std::streamoff&) call to put(...) on read-only SA");
        if (index < _start || index >= _end) throw std::out_of_range(
                "SuffixArray::put(const Eigen::Index&, const std::streamoff&) index "+
                std::to_string(index)+" out of range ["+std::to_string(_start)+", "+std::to_string(_end)+")");
        auto cacheId(index - _cachedFirstId);
        if (index < _cachedFirstId || index >= _cachedFirstId + _cacheSize)  // cache miss
            cacheId = load(index);
        _dirty = true;
        _cache[cacheId] = suffixId;
    }

    inline void slice(const std::vector<Eigen::Index>& ends) {
        if (_readOnly) throw std::runtime_error(
                "SuffixArray::slice(std::vector<Eigen::Index>&) cannot slice an array in read-only mode");
        flush();
        auto start(_start);
        auto srcFile(open(_start));
        for (const Eigen::Index& end : ends) {
            std::ofstream file(getFilename(start, end), std::ios::binary | std::ios::out);
            auto offset((end - start) % _cacheSize);
            for (Eigen::Index i(0); i < (end - start)/_cacheSize; ++i) {
                srcFile.read(reinterpret_cast<char*>(&_cache[0]), buffSize());
                file.write(reinterpret_cast<char*>(&_cache[0]), buffSize());
            }
            srcFile.read(reinterpret_cast<char*>(&_cache[0]), offset*sizeof(std::streamoff));
            file.write(reinterpret_cast<char*>(&_cache[0]), offset*sizeof(std::streamoff));
            file.close();
            start = end;
        }
        srcFile.close();
    }

    [[nodiscard]] inline std::string getFilename() const { return getFilename(_start, _end); }
    [[nodiscard]] inline std::string getBasename() const { return _basename; }
    [[nodiscard]] constexpr const Eigen::Index& cacheSize() const { return _cacheSize; }
    [[nodiscard]] constexpr const bool& readOnly() const { return _readOnly; }
    [[nodiscard]] constexpr Eigen::Index size() const { return _end - _start; }
    [[nodiscard]] constexpr const Eigen::Index& start() const { return _start; }
    [[nodiscard]] constexpr const Eigen::Index& end() const { return _end; }
};



#endif //CPPMODULES_SUFFIXARRAY_HPP
