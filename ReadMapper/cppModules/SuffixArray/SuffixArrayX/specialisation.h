#ifndef CPPMODULES_SPECIALISATION_H
#define CPPMODULES_SPECIALISATION_H

#include "SuffixArray.hpp"

namespace CSuffixArray {
    using CSuffixArray = SuffixArray<>;
}

#endif //CPPMODULES_SPECIALISATION_H
