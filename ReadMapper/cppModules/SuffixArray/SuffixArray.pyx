# distutils: language=c++
import os

import numpy as np
cimport numpy as np

from libcpp.string cimport string
from libcpp cimport bool
from SuffixArray cimport SuffixArray, CSuffixArray



cdef class SuffixArray:

    def __init__(self, saBasename, start, end, cacheSize=80000, readOnly=True, init=False):
        cdef bool ro = readOnly and not init
        self.__CSuffixArray = CSuffixArray(
            <string>saBasename.encode(), <long>start, <long>end, <long>(cacheSize), ro)
        self.__CSuffixArray.open(<bool>init)

    @property
    def filename(self): return self.__CSuffixArray.getFilename().decode()

    @property
    def start(self): return self.__CSuffixArray.start()
    @property
    def end(self): return self.__CSuffixArray.end()
    @property
    def size(self): return int(<long>self.__CSuffixArray.size())
    def __len__(self): return int(<long>self.__CSuffixArray.size())

    cpdef list split(self, np.ndarray ends):
        result = []
        basename = self.__CSuffixArray.getBasename().decode()
        filename = self.__CSuffixArray.getFilename().decode()
        cdef bool readOnly = self.__CSuffixArray.readOnly()
        cdef long cacheSize = self.__CSuffixArray.cacheSize()
        cdef long start = self.__CSuffixArray.start()
        if ends.size == 1:
            return [(basename, start, ends[0], cacheSize, readOnly, False)]
        cdef vector[long] slices
        for end in ends:
            slices.push_back(end)
        self.__CSuffixArray.slice(slices)
        cdef size_t i
        for i in range(slices.size()):
            result.append((basename, start, slices[i], cacheSize, readOnly, False))
            start = slices[i]
        os.remove(filename)
        return result

    cpdef long get(self, const long& index):
        return self.__CSuffixArray.get(index)

    cpdef void put(self, const long& index, long suffixId):
        self.__CSuffixArray.put(index, suffixId)


    def __getitem__(self, const long& index): return self.get(index)
    def __setitem__(self, const long& index, long suffixId): self.put(index, suffixId)

    def __iter__(self):
        cdef long i
        for i in range(self.start, self.end):
            yield self.get(i)


