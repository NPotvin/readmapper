# distutils: language=c++

import numpy as np
cimport numpy as np
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp cimport bool


cdef extern from "specialisation.h" namespace "CSuffixArray" nogil:
    cdef cppclass CSuffixArray:
        inline CSuffixArray() nogil
        inline CSuffixArray(string filename, const long& start, const long& end, const long& cacheSize,
                       const bool& readOnly) nogil
        inline CSuffixArray(const CSuffixArray& other) nogil
        inline CSuffixArray(CSuffixArray&& other) nogil

        inline CSuffixArray& operator=(const CSuffixArray& other) nogil
        inline CSuffixArray& operator=(CSuffixArray&& other) nogil

        inline const long& get(const long& index) nogil except+
        inline void put(const long& index, const long& suffixId) nogil except+
        inline void slice(const vector[long]& ends) nogil except+
        inline string getFilename() nogil const
        inline string getBasename() nogil const
        inline const long& cacheSize() nogil const
        inline const bool& readOnly() nogil const
        inline long size() nogil const
        inline const long& start() nogil const
        inline const long& end() nogil const
        inline void open(const bool& initialize) nogil except+


cdef class SuffixArray:
    cdef CSuffixArray __CSuffixArray

    cpdef list split(self, np.ndarray ends)

    cpdef long get(self, const long& index)
    cpdef void put(self, const long& index, const long& suffixId)






