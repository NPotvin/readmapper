# distutils: language=c++

from libcpp.string cimport string
import numpy as np
cimport numpy as np


cdef extern from "specialisation.h" namespace "CSuffix" nogil:
    cdef cppclass CSuffix:
        inline CSuffix() nogil
        inline CSuffix(string filename, const long& cacheSize) nogil
        inline CSuffix(const CSuffix& other) nogil
        inline CSuffix(CSuffix&& other) nogil

        inline CSuffix& operator=(const CSuffix& other) nogil
        inline CSuffix& operator=(CSuffix&& other) nogil

        # inline void open(string filename, const long& cacheSize) nogil
        inline const char* get(const long& index, const long& length) nogil except+
        inline const char* get(const long& index) nogil except+
        inline const long& size() nogil const
        inline string filename() nogil const
        inline void open() nogil
        inline void close() nogil


cdef class Suffix:
    cdef CSuffix __CSuffix

    cpdef np.ndarray getView(self, const long& index, long length)
    cpdef np.ndarray getWord(self, const long& index, long length)
    cpdef char get(self, const long& index)
    cpdef void close(self)





