#ifndef MODULES_SPECIALISATION_H
#define MODULES_SPECIALISATION_H

#include "Suffix.hpp"

namespace CSuffix {
    using CSuffix = Suffix<80000000>;
}

#endif //MODULES_SPECIALISATION_H
