#ifndef MODULES_SUFFIX_HPP
#define MODULES_SUFFIX_HPP

#include <Eigen/Eigen>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <sys/stat.h>
#include <unistd.h>


/**
 * Finds the exact size of a file.
 * NOTE: it relies on C utilities rather than the
 *      C++ streams since streams do not give the
 *      exact size when working on large files
 * @param filename a C string, the name of the file
 * @return the size of the file
 */
inline std::ptrdiff_t filesize(const char* filename) {
    FILE* file = fopen(filename, "r");
    if (!file) return -1;
    fseek(file, 0, SEEK_END);
    auto size(ftell(file));
    fclose(file);
    return size;
}


template <Eigen::Index CacheSize=Eigen::Dynamic>
class Suffix {
private:

    using Cache = Eigen::Vector<char, CacheSize==0 ? Eigen::Dynamic : CacheSize>;
    std::string _filename;
    std::ifstream _file;
    std::ptrdiff_t _size;
    Cache _cache;
    Eigen::Index _cachedIndex;
    Eigen::Index _cachedEnd;

    [[nodiscard]] constexpr Eigen::Index cacheSize() const { return _cache.rows(); }

    constexpr Eigen::Index read(const Eigen::Index& id, const std::streamoff& fpos, const std::streamsize& length) {
        if (!_file.is_open()) open();
        _file.seekg(fpos);
        auto availableSpace(cacheSize()-id);
        auto readSize(_file.readsome(&_cache[id], availableSpace));
        if (!_file.eof()) {
            if (readSize >= length) return readSize;
            _file.read(&_cache[id + readSize], length-readSize);
            if (!_file.eof()) return length;
        }
        _file.clear();
        readSize = _size - fpos;
        auto uninitialisedLength(length - readSize);
        memset(&_cache[id + readSize], '\0', uninitialisedLength);
        return length;
    }

    constexpr void initCache() {
        if (_cachedIndex == _cachedEnd) {
            _cachedIndex = 0;
            _cachedEnd = read(0, 0, 1);
        }
    }

    /**
     * Relocates data in the cache to allow the addition of a
     *      segment of a given length beginning at index
     * @param index the index marking the beginning of the segment
     * @param length the size of the segment
     * @return Eigen::Index id: the id of the cache where new data
     *      should be added
     */
    constexpr Eigen::Index relocate(const Eigen::Index& index, const Eigen::Index& length) {
        if (index < _cachedIndex) {  // relocate to the right
            auto leftOffset(_cachedIndex - index);
            auto keptSize(_cachedEnd-_cachedIndex);
            if (keptSize + leftOffset > cacheSize()) {
                auto crop(keptSize + leftOffset - cacheSize());
                keptSize -= crop;
                _cachedEnd -= crop;
            }
            _cache.segment(leftOffset, keptSize) = _cache.head(keptSize);
            _cachedIndex = index;
            return 0;
        }
        if (index+length - _cachedIndex > cacheSize()) {  // relocate to the left
            auto crop(index+length - _cachedIndex - cacheSize());
            auto keptSize(_cachedEnd - _cachedIndex - crop);
            _cache.head(keptSize) = _cache.segment(crop, keptSize);
            _cachedIndex += crop;
            return keptSize;
        }
        return index - _cachedIndex;
    }

    constexpr const char* handleFullCacheMiss(const std::streamoff& index, const Eigen::Index& length) {
        auto holeStart(std::min(index+length, _cachedEnd));
        auto holeLength(std::max(index, _cachedIndex) - holeStart);
        if (holeLength + _cachedEnd-_cachedIndex + length <= cacheSize()) {  // filling the hole
            holeStart = std::min(index, holeStart);
            holeLength += length;
            auto cacheReadId(relocate(holeStart, holeLength));
            auto readSize(read(cacheReadId, holeStart, holeLength));
            _cachedEnd = std::min(_size, std::max(_cachedEnd, _cachedEnd+readSize));
            return &_cache[index - _cachedIndex];
        }
        _cachedIndex = index;
        _cachedEnd = std::min(_size, index + read(0, index, length));
        return &_cache[0];
    }


public:


    inline Suffix() : _filename(), _file(), _size(-1), _cache(), _cachedIndex(0), _cachedEnd(0) {}
    inline Suffix(std::string genomeFile, const Eigen::Index& cacheSize) :
            _filename(std::move(genomeFile)), _file(_filename, std::ios::in|std::ios::binary),
            _size(filesize(genomeFile.c_str())), _cache(CacheSize==Eigen::Dynamic ? cacheSize : CacheSize),
            _cachedIndex(0), _cachedEnd(0) { initCache(); }
    inline Suffix(const Suffix& other) :
            _filename(other._filename), _file(), _size(other._size != -1 ? other._size : filesize(_filename.c_str())),
            _cache(other._cache), _cachedIndex(other._cachedIndex), _cachedEnd(other._cachedEnd) { initCache(); }
    inline Suffix(Suffix&& other) noexcept :
            _filename(std::move(other._filename)), _file(),
            _size(other._size != -1 ? std::move(other._size) : filesize(_filename.c_str())),
            _cache(std::move(other._cache)), _cachedIndex(std::move(other._cachedIndex)),
            _cachedEnd(std::move(other._cachedEnd)) { initCache(); }
    ~Suffix() { close(); }


    inline Suffix& operator=(const Suffix& other) {
        _filename = other._filename;
        _size = other._size != -1 ? other._size : filesize(_filename.c_str());
        _cache = other._cache;
        _cachedIndex = other._cachedIndex;
        _cachedEnd = other._cachedEnd;
        initCache();
        return *this;
    }
    inline Suffix& operator=(Suffix&& other)  noexcept {
        _filename = std::move(other._filename);
        _size = other._size != -1 ? std::move(other._size) : filesize(_filename.c_str());
        _cache = std::move(other._cache);
        _cachedIndex = std::move(other._cachedIndex);
        _cachedEnd = std::move(other._cachedEnd);
        initCache();
        return *this;
    }

    constexpr const char* get(const std::streamoff& index) { return get(index, 1); }
    constexpr const char* get(const std::streamoff& index, const Eigen::Index& length) {
        if constexpr (CacheSize != Eigen::Dynamic) if (length > CacheSize)
                throw std::out_of_range("overflow in Suffix::get(const std::streamoff&, const Eigen::Index&)");
        if (index >= _cachedIndex && index + length < _cachedEnd)  //cache hit -> early return
            return &_cache[index - _cachedIndex];
        // cache miss
        if constexpr (CacheSize == Eigen::Dynamic) if (length > cacheSize()) _cache.conservativeResize(length);
        if (index+length < _cachedIndex || index > _cachedEnd)  // full cache miss
            return handleFullCacheMiss(index, length);
        if (index < _cachedIndex) {
            auto segmentSize(_cachedIndex - index);
            auto cacheId(relocate(index, segmentSize));
            auto readSize(read(cacheId, index, segmentSize));
            _cachedEnd = std::min(_size, std::max(_cachedEnd, index + readSize));
        }
        if (index + length > _cachedEnd) {
            auto segmentSize(index+length - _cachedEnd);
            auto tmpIndex(_cachedEnd);
            auto cacheId(relocate(tmpIndex, segmentSize));
            auto readSize(read(cacheId, tmpIndex, segmentSize));
            _cachedEnd = std::min(_size, std::max(_cachedEnd, tmpIndex + readSize));
        }
        return &_cache[index - _cachedIndex];
    }

    [[nodiscard]] inline std::string filename() const { return _filename; }
    [[nodiscard]] constexpr const std::ptrdiff_t& size() const { return _size; }

    constexpr void open() { _file = std::ifstream(_filename, std::ios::in|std::ios::binary); }
    constexpr void close() { if (_file.is_open()) _file.close(); }
};


#endif //MODULES_SUFFIX_HPP
