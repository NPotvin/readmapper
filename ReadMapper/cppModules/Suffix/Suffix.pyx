# distutils: language=c++

from libcpp.string cimport string
import numpy as np
cimport numpy as np
from Suffix cimport Suffix, CSuffix



cdef class Suffix:

    def __init__(self, genomeFile, cacheSize=160000000):
        self.__CSuffix = CSuffix(<string>genomeFile.encode(), <long>(cacheSize))

    @property
    def size(self): return int(<long>self.__CSuffix.size())
    @property
    def filename(self): return self.__CSuffix.filename().decode()

    cpdef void close(self): self.__CSuffix.close()

    cpdef np.ndarray getView(self, const long& index, long length):
        cdef np.ndarray[const char, ndim=1, mode='c'] result = np.ndarray(
            shape=(length,), buffer=<const char[:length]>self.__CSuffix.get(index, length), dtype=np.byte, order='C'
        )
        return result

    cpdef np.ndarray getWord(self, const long& index, long length):
        return np.array(self.getView(index, length), dtype=np.int32)

    cpdef char get(self, const long& index):
        cdef np.ndarray[const char, ndim=1, mode='c'] result = np.ndarray(
            shape=(1,), buffer=<const char[:1]>self.__CSuffix.get(index), dtype=np.byte, order='C'
        )
        return result[0]


