from cppModules import setupModules
setupModules(override=False, debug=False)
from test import *


if __name__ == "__main__":

    # testRadixSort()
    # testRadixSortTwoSteps()
    # testHybridRadixSort()
    # testFourStepsRadixSort()
    # testSkew()
    testIndexing()
    # testCSuffix()
    # testCSuffix16()
