#!usr/bin/python3
from cppModules import setupModules
from utils.Base import Base

setupModules(override=False, debug=False)

import os
import re
import numpy as np
from utils.checkSort import checkSort
from cppModules.Suffix.Suffix80 import Suffix as Suffix80
from cppModules.Suffix.Suffix160 import Suffix as Suffix160
from cppModules.Suffix.SuffixX import Suffix
from cppModules.SuffixArray.SuffixArrayX import SuffixArray
from utils.fileHandling import setupGenomeFiles, recoverSlices, parseReads
from algorithms.genomeIndexing import indexGenome
from algorithms.star import star
from DataStructures.GenomeIndex import GenomeIndex


def buildSAFiles(genomeFile, destPath="mapData", resetGenomeFile=False, cache=160, check=False, maxThreads=os.cpu_count() - 1):
    print("\nSetting up files ...", end="", flush=True)
    _, ref = os.path.split(genomeFile)
    ref = re.sub(r"\..*", "", ref)
    destFile = os.path.join(destPath, ref, "Index")
    chrIndexFilename, genomeFilename, arraySlicesFilebase = \
        setupGenomeFiles(genomeFile, destFile, override=resetGenomeFile, cleanArrays=True)
    print("\rSetting up files: Done", flush=True)
    if cache <= 80: suffix = Suffix80(genomeFilename)
    elif cache <= 160: suffix = Suffix160(genomeFilename)
    else: suffix = Suffix(genomeFilename, cacheSize=cache*10**6)
    sa = SuffixArray(arraySlicesFilebase, 0, suffix.size, init=True, cacheSize=cache*10**6)
    print("Indexing Genome ...", end="", flush=True)
    indexGenome(suffix, prefixLen=70, preIndPrefLen=12, sa=sa, recursDepth=2, sliceDepth=3, maxThreads=maxThreads)
    print("\rIndexing Genome: Done", flush=True)
    if check:
        print("Recovering slices ...", end="", flush=True)
        slices = recoverSlices(arraySlicesFilebase)
        print("\rRecovering slices: Done", flush=True)
        ends = np.array([end for _, _, end, _, _, _ in slices], dtype=np.int32)
        print("Checking ...", end="", flush=True)
        checkSort(suffix, arraySlicesFilebase, ends, 70)
        print("\rChecking: Done", flush=True)


def toStr(read, alignments):
    aligned = np.zeros(read.sequence.size, dtype=np.int32)
    locs = np.full(read.sequence.size, fill_value=-1, dtype=np.int32)
    borders = np.zeros(read.sequence.size, dtype=np.int32)
    bestAlignment = alignments[0][1]
    r = bestAlignment[:, 1]
    g = bestAlignment[:, 0]
    borders[r[0]] = 1
    borders[r[-1]] = 1
    locs[r] = g
    aligned[r] = np.array([index.word(l, 1)[0] for l in g], dtype=np.int32)
    aligned = aligned.reshape(read.sequence.shape)
    locs = locs.reshape(read.sequence.shape)
    borders = borders.reshape(read.sequence.shape)
    readLine, matchLine, genomeLine, locsLine, rIdLine = "", "", "", "", ""
    result = None
    rev = 1
    for i in range(2):
        for j in range(read.size):
            if borders[i, j]:
                if len(readLine) < len(locsLine):
                    matchLine = f"{matchLine}{' '*(len(locsLine)-len(readLine))}"
                    genomeLine = f"{genomeLine}{' '*(len(locsLine)-len(readLine))}"
                    readLine = f"{readLine}{' '*(len(locsLine)-len(readLine))}"
                if len(rIdLine) < len(locsLine):
                    rIdLine = f"{rIdLine}{' ' * (len(locsLine) - len(rIdLine))}"
                readLine, matchLine, genomeLine = f"{readLine}|", f"{matchLine}|", f"{genomeLine}|"
                locsLine, rIdLine = f"{locsLine}|{locs[i, j]}", f"{rIdLine}|{rev*j}"
            readLine = f"{readLine}{Base(read.sequence[i, j]).name}"
            m = ' '
            if aligned[i, j] == read.sequence[i, j]:
                m = ':'
            elif aligned[i, j]:
                m = "."
            matchLine = f"{matchLine}{m}"
            genomeLine = f"{genomeLine}{Base(aligned[i, j]).name if aligned[i, j] else '*'}"
            locsLine = f"{locsLine}{' ' if len(locsLine) < len(genomeLine) else ''}"
            rIdLine = f"{rIdLine}{' ' if len(rIdLine) < len(genomeLine) else ''}"
        if result is None:
            result = f"{read.readId}\n{rIdLine}\n{readLine} ...\n{matchLine}\n{genomeLine} ...\n{locsLine}\n"
            readLine, matchLine, genomeLine, locsLine, rIdLine = "", "", "", "", ""
            rev = -1
        else:
            result=f"{result}\n     {rIdLine}\n ... {readLine}\n     {matchLine}\n ... {genomeLine}\n     {locsLine}\n"
    return result



if __name__ == "__main__":
    buildSAFiles("data/chr2L.fa.gz", cache=80, check=True)
    #buildSAFiles("data/dm6.fa.gz", cache=160, check=True)

    index = GenomeIndex(12, 70, "mapData/chr2L/Index", cache=80)
    reads = parseReads("data/10000_R1.fastq", "data/10000_R2.fastq")
    print("Aligning")
    for read, alignments in star(index, reads,
                                 anchorMaxMap=30, windowSize=70, k=6, iOP=2, iEP=1, dOP=2, dEP=1):
        for alignment in alignments[:5]:
            res = toStr(read, alignment)
            with open("mapData/chr2L/output.txt", "at") as f:
                f.write(res)
                f.write('\n')
            print(res)









