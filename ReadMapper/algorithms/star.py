import numba as nb
import numpy as np
from scipy.sparse import lil_matrix


def handleAnchor(locations, previousWindows, windowSize, r, rLength, gSize):
    windows = []
    newPrevious = []  # will contain updated current windows
    extendedWindows = np.zeros(len(previousWindows), dtype=np.bool)  # tracking (non-)updated windows
    for loc in locations:
        locMerged = False  # need to know if location belongs in a previous window or not
        for j, window in enumerate(previousWindows):
            # the anchor must come after the previous window (no overlap) -> r >= window[1, 1]
            # the anchor must begin in the area after the last anchor, but before the window's end
            if r >= window[1, 1] and window[0, 1] - windowSize <= loc < window[0, 1] + windowSize:
                cpy = np.copy(window)  # avoid side effects
                cpy[0, 1] = loc + rLength + windowSize  # update window's borders
                cpy[1, 1] = r + rLength  # update covered read indices (!! range covered by anchors only)
                newPrevious.append(cpy)
                extendedWindows[j] = True  # this window has been updated, worth keeping it at hand
                locMerged = True  # anchor is added in a window, no need to create a new one
        if not locMerged:  # anchor is still not in a window, create a new one for it
            newPrevious.append(np.array([
                [max(0, loc - windowSize), min(gSize, loc + rLength + windowSize)],
                [r, r + rLength]
            ], dtype=np.int32))
    for j in np.where(np.logical_not(extendedWindows))[0]:
        # all windows that have not been updated are removed from "current windows" lists and added to
        #       "final windows" list
        windows.append(previousWindows[j])
    return windows, newPrevious


def findSeeds(gIndex, read, anchorMaxMap, windowSize, k):
    windows = []
    previousWindows = []
    seeds = []
    r = 0
    for sequence in read.sequence:  # treat forward and reverse as a single sequence (as in STAR)
        i = 0
        while i < read.size:  # for each matched areas in sequence
            locations, length = gIndex.locate(sequence[i:], read.size - i)  # find max map on genome
            if length >= k:  # worth keeping, treating the map as a seed
                if locations.size <= anchorMaxMap:  # seed can be used as anchor
                    newWindows, previousWindows = handleAnchor(
                        locations, previousWindows, windowSize, r, length, gIndex.size)
                    windows.extend(newWindows)
                for loc in locations:  # record the seeds
                    seeds.append((loc, r, length))
                r += length
                i += length
            else:
                i += 1
                r += 1
        previousWindows = windows  # moving on to reverse sequence. some overlap is allowed,
        windows = []  # considering all windows as updatable (most will be placed in windows at first iteration)
    windows = np.array(previousWindows, dtype=np.int32)
    # updating the borders in the read to ease computation of acceptable seeds later:
    if windows.size > 0:
        windows[:, 1, 0] = np.maximum(windows[:, 1, 0]-windowSize, 0)
        windows[:, 1, 1] = np.minimum(windows[:, 1, 1]+windowSize, 2*read.size)
    return seeds, windows


def cluster(gIndex, read, anchorMaxMap, windowSize, k):
    seeds, windows = findSeeds(gIndex, read, anchorMaxMap, windowSize, k)
    matrix = lil_matrix((gIndex.size+1, read.size*2+1), dtype=np.int32)  # +1 for padding in the alignment computation
    if windows.size == 0: return matrix, windows
    for g, r, l in seeds:
        if np.any(np.logical_or(np.logical_and(  # the seeds that fit in a window are added (the others
                np.logical_and(windows[:, 0, 0] <= g, windows[:, 0, 1] > g),  #                 are discarded)
                np.logical_and(windows[:, 1, 0] <= r, windows[:, 1, 1] > r)
        ), np.logical_and(
                np.logical_and(windows[:, 0, 0] <= g+l, windows[:, 0, 1] > g+l),
                np.logical_and(windows[:, 1, 0] <= r+l, windows[:, 1, 1] > r+l)
        ))):
            matrix[np.arange(g, g+l, dtype=np.int32), np.arange(r, r+l, dtype=np.int32)] = 1
    return matrix, windows


def computeMatchScores(gIndex, matchMatrix, windows, fullSequence):
    for window in windows:
        rWord = fullSequence[window[1, 0]: window[1, 1]]
        gWord = gIndex.word(window[0, 0], window[0, 1]-window[0, 0])
        matches = np.equal(gWord[:, np.newaxis], rWord[np.newaxis, :])
        # previously matched seed indeed match the genome
        matchMatrix[window[0, 0]: window[0, 1], window[1, 0]: window[1, 1]] = np.where(matches, 1, -1)


def scoreAlignment(window, matchMatrix, iOP, iEP, dOP, dEP):
    r = np.arange(window[1, 0], window[1, 1], dtype=np.int32)
    g = np.arange(window[0, 0], window[0, 1], dtype=np.int32)
    V = np.zeros((g.size+1, r.size+1), dtype=np.int32)
    W = np.zeros((g.size+1, r.size+1), dtype=np.int32)
    S = np.zeros((g.size+1, r.size+1), dtype=np.int32)
    # the reason of the +1 is to pad the matrices with 0 at -1 indices
    #   (using the "wrap around" property of python)
    for i in range(g.size):
        V[i, :] = np.maximum(S[i-1, :]-dOP, V[i-1, :]-dEP)
        for j in range(r.size):
            W[i, j] = max(S[i, j-1]-iOP, W[i, j-1]-iEP)
            S[i, j] = max(S[i-1, j-1]+matchMatrix[g[i], r[j]], V[i, j], W[i, j], 0)
    return S, V, W, g, r


def backtrack(S, V, W, M, g, r, i, j, score, alignment, mask, extend=0):
    alignments = []
    push = lambda k, l, align, sc, msk, ext: alignments.extend(backtrack(
        S, V, W, M, g, r, k, l, sc, np.concatenate((np.array([[g[k], r[l]]], dtype=np.int32), align)), msk, ext))
    prevIds = ((i - 1, j - 1), (i - 1, j), (i, j - 1))
    prev = np.array([S[i-1, j-1]+M[i, j], V[i, j], W[i, j]], np.int32)
    allowedMax = np.max(prev[mask])
    if allowedMax:
        indices = np.where(np.logical_and(mask, prev == allowedMax))[0]
        for id in indices:
            i, j = prevIds[id]
            if extend and id != extend:
                cpyMask = np.copy(mask)
                cpyMask[extend] = False
                push(i, j, alignment, score, cpyMask, id)
            else:
                push(i, j, alignment, score, mask, id)
    else:
        alignments.append((score, alignment))
    return alignments


def stitch(matchMatrix, windows, iOP, iEP, dOP, dEP):
    alignments = []
    for window in windows:
        alignments.append([])
        S, V, W, g, r = scoreAlignment(window, matchMatrix, iOP, iEP, dOP, dEP)
        for i, j in zip(*np.where(S != 0)):
            if np.count_nonzero(S[i: i+2, j: j+2]) == 1:  # an "end of alignment"
                alignments[-1].extend(backtrack(S, V, W, matchMatrix,
                    g, r, i, j, S[i, j], np.array([[g[i], r[j]]], dtype=np.int32), np.ones(3, dtype=np.bool)
                ))
    return alignments


def star(gIndex, reads, anchorMaxMap=20, windowSize=20, k=12, iOP=4, iEP=1, dOP=4, dEP=1, scoreRange=.1):
    for read in reads:
        print("Clustering ...", end="", flush=True)
        matchMatrix, windows = cluster(gIndex, read, anchorMaxMap, windowSize, k)
        print("\rClustering: Done\nScoring ...", end="", flush=True)
        computeMatchScores(gIndex, matchMatrix, windows, read.fullSequence)
        print("\rScoring: Done\nStitching ...", end="", flush=True)
        alignments = stitch(matchMatrix, windows, iOP, iEP, dOP, dEP)
        print("\rStitching: Done\nPost-processing ...", end="", flush=True)
        results = []
        for window in alignments:
            if len(window):
                window.sort(key=lambda elem: elem[0], reverse=True)
                j = 1
                while j < len(window) and window[j][0] > (1-scoreRange)*window[0][0]: j += 1
                results.append(window[:j])
        print("\rPost-processing: Done\n", flush=True)
        if len(results):
            results.sort(key=lambda elem: elem[0][0], reverse=True)
            j = 1
            while j < len(results) and results[j][0][0] > (1-scoreRange)*results[0][0][0]: j += 1
            results = results[:j]
            yield read, results


