import numba as nb
import numpy as np
from utils.Base import Base


@nb.njit("int32[:](int32[:])")
def pad(text):
    return np.concatenate((text, np.zeros(3, dtype=np.int32)))


@nb.njit("int32(int32[:], int32[:], int32[:], int32, int32)")
def rankS12(s12, sa12, text, n0, n02):
    rank, triple = 0, np.full(3, fill_value=-1, dtype=np.int32)
    for i in range(n02):
        if np.any(triple != text[sa12[i]: sa12[i]+3]):
            rank += 1
            triple = text[sa12[i]: sa12[i]+3]
        if sa12[i]%3 == 1:
            s12[sa12[i]//3] = rank
        else:
            s12[sa12[i]//3 + n0] = rank
    return rank


@nb.njit("void(int32[:], int32[:], int32[:], int32, int32)")
def radix1(ids, sa, text, length, alphabetLen):
    buckets = np.zeros(alphabetLen+2, dtype=np.int32)
    for i in range(length):
        buckets[text[ids[i]]+1] += 1
    counts = np.cumsum(buckets)
    for i in range(length):
        sa[counts[text[ids[i]]]] = ids[i]
        counts[text[ids[i]]] += 1


@nb.njit("void(int32[:], int32[:], int32[:], int32, int32)")
def radix3(ids, sa, text, length, alphabetLen):
    # lsb first
    radix1(ids, sa, text[2:], length, alphabetLen)
    radix1(sa, ids, text[1:], length, alphabetLen)
    radix1(ids, sa, text, length, alphabetLen)


@nb.njit("void(int32[:], int32[:], int32[:], int32[:], int32[:], int32, int32, int32, int32)")
def mergePartialSAs(sa0, sa12, s12, sa, text, n0, n1, n02, length):
    p, t = 0, n0-n1
    k = 0
    while k < length:
        j = sa0[p]
        if sa12[t] < n0:
            i = 3*sa12[t] + 1
            leq = (text[i], s12[sa12[t]+n0]) <= (text[j], s12[j//3])
        else:
            i = 3*(sa12[t] - n0) + 2
            leq = (text[i], text[i+1], s12[sa12[t]-n0+1]) <= (text[j], text[j+1], s12[j//3+n0])
        if leq:
            sa[k] = i
            t += 1
            if t == n02:
                sa[k+1: length] = sa0[p: n0]
                k = length
        else:
            sa[k] = j
            p += 1
            if p == n0:
                sa[k+1: length] = np.where(sa12[t: n02] < n0, 3*sa12[t: n02]+1, 3*(sa12[t: n02]-n0)+2)
                k = length
        k += 1


@nb.njit("Tuple((int32, int32, int32, int32[:], int32[:], int32[:], int32[:]))(int32)")
def initWorkingVariables(length):
    n0 = (length+2)//3
    n1 = (length+1)//3
    n2 = length//3
    n02 = n0+n2
    s12 = np.zeros(n02+3, dtype=np.int32)
    sa12 = np.zeros(n02+3, dtype=np.int32)
    s0 = np.zeros(n0, dtype=np.int32)
    sa0 = np.zeros(n0, dtype=np.int32)
    s12[:2*(length+n0-n1)//3] = np.array([i for i in range(length+n0-n1) if i%3])
    return n0, n1, n02, s12, sa12, s0, sa0


@nb.njit("void(int32[:], int32[:], int32, int32)")
def skew(text, sa, length, alphabetLen):
    n0, n1, n02, s12, sa12, s0, sa0 = initWorkingVariables(length)
    radix3(s12, sa12, text, n02, alphabetLen)
    # rank the indices of arrays 1 and 2
    lastRank = rankS12(s12, sa12, text, n0, n02)
    # determine the indices of s12
    if lastRank < n02:
        # sorting is not finished yet, needs to recurse to get a fitting sa12
        skew(s12, sa12, n02, lastRank)
        s12[sa12[:n02]] = np.arange(1, n02+1, dtype=np.int32)
    else:
        sa12[s12[:n02]-1] = np.arange(n02, dtype=np.int32)
    # sort the 0 array suffixes
    s0[:] = 3*sa12[:n02][sa12[:n02] < n0]
    radix1(s0, sa0, text, n0, alphabetLen)
    # merge the 0 array with the ones for 1 and 2
    mergePartialSAs(sa0, sa12, s12, sa, text, n0, n1, n02, length)


def readMapSkew(suffix, prefixLength, sa, offset, start, end):
    saSize = end - start
    length = prefixLength - offset + 3-(prefixLength-offset)%3
    text = np.concatenate([suffix.getWord(sa[i]+offset, length) for i in range(start, end)])
    indices = np.empty(length*saSize, dtype=np.int32)
    skew(pad(text), indices, length*saSize, len(Base))
    indices = indices[indices % length == 0]
    indices = indices//length
    newSA = np.array([sa[start+i] for i in indices], dtype=np.int32)
    for i in range(saSize):
        sa.put(start+i, newSA[i])

