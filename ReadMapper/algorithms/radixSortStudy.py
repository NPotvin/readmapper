import numpy as np
from utils.Base import Base
from .radixSort import twoStepsRadixSort
from cppModules.SuffixArray.SuffixArrayX import SuffixArray


def radixSwapUpdateBorder(buckId, suffix, borders, ends, sa, offset):
    while borders[buckId] < ends[buckId] and \
            suffix.get(sa[borders[buckId]] + offset) == buckId:
        # find the next intruder in this bucket ("this bucket" == bucket at bucketId)
        borders[buckId] += 1


def radixRearrange(suffix, ends, sa, offset):
    # (for each bucket) borderIds contains the index of the first element not belonging in 'this' bucket
    swapNum = 0
    borderIds = np.empty(ends.size, dtype=np.int32)
    borderIds[0] = sa.start
    borderIds[1:] = ends[:-1]  # initialised to point to the first elem of each bucket
    indices = np.where(borderIds != ends)[0]  # indices of still unsorted buckets
    updateBorders = lambda buckId: radixSwapUpdateBorder(buckId, suffix, borderIds, ends, sa, offset)  # shortcut
    for i in indices: updateBorders(i)
    indices = np.where(borderIds != ends)[0]  # indices of still unsorted buckets
    while len(indices):
        for buckId in indices:
            if borderIds[buckId] < ends[buckId]:  # if this bucket has an intruder
                destBuckId = suffix.get(sa[borderIds[buckId]] + offset)  # get id of destination bucket
                while destBuckId != buckId:  # while current intruder does not belong in this bucket
                    save = sa[borderIds[buckId]]
                    sa.put(borderIds[buckId], sa[borderIds[destBuckId]])  # update intruder
                    sa.put(borderIds[destBuckId], save)
                    swapNum += 1
                    borderIds[destBuckId] += 1  # increment border since we placed an element in the right place
                    updateBorders(destBuckId)
                    destBuckId = suffix.get(sa[borderIds[buckId]] + offset)  # update destination bucket of
                borderIds[buckId] += 1  # update the size                           # the new intruder
                updateBorders(buckId)
        indices = np.where(borderIds != ends)[0]
    return swapNum


def computeEnds(suffix, sa, offset):
    ends = np.zeros(len(Base)+1, dtype=np.int32)
    for i in sa:
        j = suffix.get(i + offset)  # the index of sArr[i]'s bucket, ends[j] is where it will be inserted
        ends[j] += 1
    nonEmpty = np.where(ends != 0)[0]  # getting non-empty buckets indices
    ends = np.cumsum(ends)
    ends += sa.start
    return ends, nonEmpty


def radixSort(suffix, prefixLength, sa, offset=0):
    if sa.size == 1: return np.array([sa.end], dtype=np.int32), 0
    ends, nonEmpty = computeEnds(suffix, sa, offset)
    swapNum = 0
    if nonEmpty.size > 1:
        swapNum += radixRearrange(suffix, ends, sa, offset)
    ends = ends[nonEmpty]
    slices = sa.split(ends)
    if offset+1 < prefixLength:
        results = [radixSort(suffix, prefixLength, SuffixArray(*slice), offset + 1) for slice in slices]
        ends = np.concatenate([res for res, _ in results])
        for _, res in results:
            swapNum += res
    return ends, swapNum


def hybridRadixSort(suffix, prefixLength, sa, recursDepth=1, offset=0):
    if sa.size == 1: return np.array([sa.end], dtype=np.int32), 0
    if offset < recursDepth:
        ends, nonEmpty = computeEnds(suffix, sa, offset)
        swapNum = 0
        if nonEmpty.size > 1:
            swapNum += radixRearrange(suffix, ends, sa, offset)
        ends = ends[nonEmpty]
        slices = sa.split(ends)
        if offset+1 < prefixLength:
            results = [
                hybridRadixSort(suffix, prefixLength, SuffixArray(*slice), recursDepth, offset + 1)
                for slice in slices
            ]
            ends = np.concatenate([res for res, _ in results])
            for _, res in results:
                swapNum += res
        return ends, swapNum
    else:
        return twoStepsRadixSort(suffix, prefixLength, sa, recursDepth, slice=True)


def fourStepsRadixSort(suffix, prefixLength, sa, recursDepth=1):
    if sa.size == 1: return np.array([sa.end], dtype=np.int32), 0
    if recursDepth:
        ends, swapNum = twoStepsRadixSort(suffix, recursDepth, sa, 0)
        slices = sa.split(ends)
        results = [
            twoStepsRadixSort(suffix, prefixLength, SuffixArray(*slice), recursDepth, slice=True)
            for slice in slices
        ]
        ends = np.concatenate([res for res, _ in results])
        for _, res in results:
            swapNum += res
        return ends, swapNum
    else:
        return twoStepsRadixSort(suffix, prefixLength, sa, slice=True)









