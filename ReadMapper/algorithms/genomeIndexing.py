import os
from multiprocessing.pool import Pool
from cppModules.SuffixArray.SuffixArrayX import SuffixArray
from cppModules.Suffix.Suffix80 import Suffix
from utils.fileHandling import recordPreindexEntry
from .radixSort import twoStepsRadixSort
from .skew import readMapSkew


def sliceArray(suffix, sa, sliceDepth, offset):
    if sliceDepth <= offset:
        return (sa,), offset
    ends, _ = twoStepsRadixSort(suffix, sliceDepth, sa, offset)
    slices = sa.split(ends)
    sas = (SuffixArray(*slice) for slice in slices)
    return sas, sliceDepth


def buildPreindex(suffix, sas, preIndPrefLength, offset):
    if preIndPrefLength <= offset:
        for sa in sas:
            recordPreindexEntry(sa, sa.start)
            if sa.size > 1:
                yield sa, sa.start, sa.end, offset
    else:
        for sa in sas:
            start = sa.start
            end = sa.end
            ends, _ = twoStepsRadixSort(suffix, preIndPrefLength, sa, offset, start=start, end=end)
            for end in ends:
                recordPreindexEntry(sa, start)
                if end - start > 1:
                    yield sa, start, end, preIndPrefLength
                start = end


def parallelWorker(genomeFile, prefixLength, slice, offset, sliceDepth, preIndexPrefLen):
    sa = SuffixArray(*slice)
    if sa.size == 1:
        recordPreindexEntry(sa, sa.start)
    else:
        suffix = Suffix(genomeFile)
        sas, offset = sliceArray(suffix, sa, sliceDepth, offset)
        preIndexedArrays = buildPreindex(suffix, sas, preIndexPrefLen, offset)
        for sa, start, end, offset in preIndexedArrays:
            readMapSkew(suffix, prefixLength, sa, offset, start, end)
        suffix.close()


def indexGenome(suffix, prefixLen, preIndPrefLen, sa, recursDepth=2, sliceDepth=3, maxThreads=os.cpu_count() - 1):
    if sa.size == 1: return 0
    genomeFile = suffix.filename
    recursDepth = max(1, min(sliceDepth, recursDepth))
    maxThreads = max(1, maxThreads)
    ends, _ = twoStepsRadixSort(suffix, recursDepth, sa, 0)  # dividing work to be parallelised
    suffix.close()
    slices = sa.split(ends)
    with Pool(maxThreads) as pool:
        workers = [  # launching parallel executions
            pool.apply_async(parallelWorker, (genomeFile, prefixLen, slice, recursDepth, sliceDepth, preIndPrefLen))
            for slice in slices
        ]
        for res in workers:  # join
            res.wait()
