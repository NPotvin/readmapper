import numpy as np
from utils.Base import Base


def computeAllBuckets(suffix, sa, segmentLength, offset, start, end):
    buckets = np.zeros(tuple(len(Base)+1 for _ in range(segmentLength)), dtype=np.int32)
    for i in range(start, end):
        j = suffix.getWord(sa[i] + offset, segmentLength)  # the full indices of the bucket of prefix at index i
        buckets[tuple(j)] += 1
    return buckets


def computeEnds(suffix, sa, segmentLength, offset, start, end):
    ends = computeAllBuckets(suffix, sa, segmentLength, offset, start, end)
    nonEmpty = np.where(ends.flat != 0)[0]  # getting non-empty buckets indices
    ends = np.cumsum(ends).reshape(ends.shape)
    ends += start
    return ends, nonEmpty


def radixSwapUpdateManyBorders(buckIds, suffix, borders, ends, sa, segmentLength, offset):
    while borders[buckIds] < ends[buckIds] and \
            tuple(suffix.getWord(sa[borders[buckIds]] + offset, segmentLength)) == buckIds:
        # find the next intruder in this bucket ("this bucket" == bucket at bucketIds)
        borders[buckIds] += 1


def radixRearrangeManyBuckets(suffix, ends, sa, segmentLength, offset, start):
    swapNum = 0
    # (for each bucket) borderIds contains the index of the first element not belonging in 'this' bucket
    borderIds = np.empty(ends.size, dtype=np.int32)
    borderIds[1:] = ends.flat[:-1]  # initialised to point to the first elem of each bucket
    borderIds[0] = start
    borderIds = borderIds.reshape(ends.shape)
    indices = np.where(borderIds != ends)  # indices of still unsorted buckets
    updateBorders = lambda buckIds: radixSwapUpdateManyBorders(buckIds,  # shortcut
                                                               suffix, borderIds, ends, sa, segmentLength, offset)
    for i in zip(*indices): updateBorders(tuple(i))
    indices = np.where(borderIds != ends)  # indices of still unsorted buckets
    while len(indices[0]):
        for buckIds in zip(*indices):
            if borderIds[buckIds] < ends[buckIds]:  # if this bucket has an intruder
                destBuckIds = tuple(suffix.getWord(sa[borderIds[buckIds]] + offset, segmentLength))  # destination
                while destBuckIds != buckIds:  # while current intruder does not belong in this bucket
                    save = sa[borderIds[buckIds]]
                    sa.put(borderIds[buckIds], sa[borderIds[destBuckIds]])  # update intruder
                    sa.put(borderIds[destBuckIds], save)
                    swapNum += 1
                    borderIds[destBuckIds] += 1  # increment border since we placed an element in the right place
                    updateBorders(destBuckIds)
                    destBuckIds = tuple(
                        suffix.getWord(sa[borderIds[buckIds]] + offset, segmentLength))  # update destination bucket
                borderIds[buckIds] += 1  # update the size
                updateBorders(buckIds)
        indices = np.where(borderIds != ends)
    return swapNum


def twoStepsRadixSort(suffix, prefixLength, sa, offset=0, slice=False, start=None, end=None):
    start = sa.start if start is None else start
    end = sa.end if end is None else end
    if end-start == 1: return np.array([end], dtype=np.int32), 0
    ends, nonEmpty = computeEnds(suffix, sa, prefixLength-offset, offset, start, end)
    swapNum = radixRearrangeManyBuckets(suffix, ends, sa, prefixLength-offset, offset, start)
    ends = ends.flatten()
    ends = ends[nonEmpty]
    if slice:
        sa.split(ends)
    return ends, swapNum


