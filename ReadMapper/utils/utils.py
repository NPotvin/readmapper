import os


def createDir(path):
    """
    Creates a directory in the specified path if it does not exist already
    :param path: the directory to be created
    :return: the complete path of the newly created dir
    """
    if len(path) and not os.path.exists(path):
        base, directory = os.path.split(path)
        path = os.path.join(createDir(base), directory)
        if len(directory):
            os.mkdir(path)
    return path


def cleanDir(directory):
    """
    remove a dir and its content from the file system tree
    :param directory: the directory to remove
    :return: None
    """
    if not os.path.exists(directory):  # nothing to be done
        return
    rm_dir = []  # empty dirs to be removed (FILO)
    ls_dir = [directory]  # dirs to be treated (FILO)
    while len(ls_dir):
        # pop top of ls_dir
        current_dir = ls_dir[-1]
        ls_dir = ls_dir[:-1]
        # add to rm_dir
        rm_dir.append(current_dir)
        for item in os.listdir(current_dir):
            item = os.path.join(current_dir, item)  # update path
            if os.path.isdir(item):
                ls_dir.append(item)  # new dir to treat
            else:
                os.remove(item)  # file to remove
    for item in reversed(rm_dir):  # remove (all empty) dirs from deepest to 'root'
        os.rmdir(item)