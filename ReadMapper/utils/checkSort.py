import numpy as np
from cppModules.SuffixArray.SuffixArrayX import SuffixArray


def checkSort(suffix, saPath, ends, prefLength):
    start = 0
    previousPref = None
    for end in ends:
        sa = SuffixArray(saPath, start, end)
        currentPref = suffix.getWord(sa[start], prefLength)
        if previousPref is not None:
            k = 0
            while k < prefLength and previousPref[k] == currentPref[k]: k += 1
            assert k < prefLength and previousPref[k] < currentPref[k]
        for i in range(end-start):
            if not np.array_equal(suffix.getWord(sa[start+i], prefLength), currentPref):
                previousPref = currentPref
                currentPref = suffix.getWord(sa[start+i], prefLength)
                k = 0
                while k < prefLength and previousPref[k] == currentPref[k]: k += 1
                assert k < prefLength and previousPref[k] < currentPref[k]
        previousPref = currentPref
        start = end