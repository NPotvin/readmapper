import re
import gzip
from DataStructures.Read import Read
from utils.Base import Base
from utils.utils import *
import numpy as np



def scan(filename, pattern, nLines=1):
    """
    Generator function yielding a stream of tokens i.e.
        named patterns matched from the regex passed as argument
    (Note: now handles both gzip compressed and uncompressed files)
    :param filename: a str, the name of the file to scan
    :param pattern: a re.Pattern (compiled), the pattern of a block of text
    :param nLines: an int, the number of lines to scan at once
    :return: yields a dictionary of named patterns from the next matching lines
    """
    nLines = max(0, nLines)
    _open = gzip.open if re.match(r"^.*?\.gz$", filename) else open
    with _open(filename, "rt") as f:
        text = "".join(f.readline() for _ in range(nLines))
        while len(text):  # while end of file has not been reached (read on EOF outputs an empty string)
            match = pattern.match(text)
            if match:
                yield match.groupdict()
            text = "".join(f.readline() for _ in range(nLines))


def parseReads(forwardReadsFile, reverseReadsFile):
    """
    Generator function yielding a stream of Read objects parsed
        from .fastq files
    :param forwardReadsFile: a str, the name of the file containing
            reads in forward order
    :param reverseReadsFile: a str, the name of the file containing
            reads in reverse order
    :return: yields a stream of Read objects
    """
    fastqPattern = re.compile(
        r"^@(?P<readId>[^#\n]+)#[0-9]/[12]\n"
        r"(?P<Sequence>[NATCG]+)\n"
        r"\+[^\n]*\n"
        r"(?P<Quality>[!-~]+)\n?$"
    )
    for forwArgs, reveArgs in zip(scan(forwardReadsFile, fastqPattern, 4), scan(reverseReadsFile, fastqPattern, 4)):
        if not re.match(forwArgs["readId"], reveArgs["readId"]):
            print(f"mismatch in forward/reverse sequences: {forwArgs['readId']} and {reveArgs['readId']}")
        args = {"readId": forwArgs["readId"]}
        for pref, readArgs in (("forward", forwArgs), ("reverse", reveArgs)):
            for key, func in (("Sequence", lambda b: Base[b]), ("Quality", lambda q: ord(q))):
                args[f"{pref}{key}"] = np.array([func(e) for e in readArgs[key]], dtype=np.int32)
        yield Read(**args)


def setupGenomeFiles(genomeFile, dest, override=False, cleanArrays=True):
    """"""
    genomeOutFilename = os.path.join(dest, "genome.txt")
    chrIndexFilename = os.path.join(dest, "chromosomeIndices.txt")
    arraySlicesDir = os.path.join(dest, "slices")
    preIndexDir = os.path.join(dest, "preIndex")
    if override or cleanArrays:
        cleanDir(arraySlicesDir)
        cleanDir(preIndexDir)
    createDir(arraySlicesDir)
    if not override and os.path.exists(genomeOutFilename) and os.path.exists(chrIndexFilename):
        return chrIndexFilename, genomeOutFilename, os.path.join(arraySlicesDir, "suffixArray")
    createDir(dest)
    chrPattern = re.compile(r">(?P<chrId>chr[^\s]+)\s*")
    stopCharPattern = re.compile('>')
    _open = gzip.open if re.match(r"^.*?\.gz$", genomeFile) else open
    with _open(genomeFile, "rt") as srcFile:
        with open(genomeOutFilename, "w") as destFile:
            line = srcFile.readline()
            while not stopCharPattern.match(line[0]):
                line = srcFile.readline()
            chrId = chrPattern.match(line).group("chrId")
            size, offset = 0, 0
            with open(chrIndexFilename, "w") as chrFile:
                for line in srcFile:
                    if stopCharPattern.match(line[0]):
                        chrFile.write(f"{chrId}\t{offset}\t{size}\n")
                        chrId = chrPattern.match(line).group("chrId")
                        offset += size
                        size = 0
                    else:
                        size += destFile.write("".join(chr(Base[b].value) for b in line.strip()))
                chrFile.write(f"{chrId}\t{offset}\t{size}\n")
    return chrIndexFilename, genomeOutFilename, os.path.join(arraySlicesDir, "suffixArray")


def recoverSlices(arraySlicesFilebase, saCacheSize=160*10**6):
    """"""
    directory, basename = os.path.split(arraySlicesFilebase)
    ranges = []
    filePattern = re.compile(f"^{basename}(?P<start>[0-9]+)_(?P<end>[0-9]+)\\.txt$")
    for sliceFile in os.listdir(directory):
        match = filePattern.match(sliceFile)
        ranges.append((int(match.group("start")), int(match.group("end"))))
    ranges.sort()
    return [(arraySlicesFilebase, start, end, saCacheSize, True, False) for start, end in ranges]


def loadSA(arraySlicesFilebase, cacheSize=160*10**6):
    slices = recoverSlices(arraySlicesFilebase, cacheSize)
    if len(slices) == 1: return slices[0]
    tmpFile = f"{arraySlicesFilebase}_tmp.txt"
    totalStart, totalEnd = -1, 0
    with open(tmpFile, "wb") as dst:
        for _, start, end, _, _, _ in slices:
            if totalStart == -1: totalStart = start
            totalEnd = end
            sliceFile = f"{arraySlicesFilebase}{start}_{end}.txt"
            with open(sliceFile, "rb") as src:
                dst.write(src.read())
            os.remove(sliceFile)
    os.rename(tmpFile, f"{arraySlicesFilebase}{totalStart}_{totalEnd}.txt")
    return (arraySlicesFilebase, totalStart, totalEnd, cacheSize, True, False)


def recordPreindexEntry(sa, start):
    """"""
    preIndexDir, _ = os.path.split(sa.filename)
    preIndexDir, _ = os.path.split(preIndexDir)
    preIndexDir = createDir(os.path.join(preIndexDir, "preIndex"))
    with open(os.path.join(preIndexDir, f"preindexDump{sa.start}_{sa.end}.txt"), "at") as f:
        f.write(f"{start}\t{sa[start]}\n")


def recoverPreindex(path):
    """"""
    path = os.path.join(path, "preIndex")
    listFiles = os.listdir(path)
    entryPattern = re.compile(r"^(?P<start>[0-9]+)\t(?P<value>[0-9]+)\n?$")
    for preIndFile in listFiles:
        with open(os.path.join(path, preIndFile), "rt") as pif:
            for line in pif:
                match = entryPattern.match(line)
                yield int(match.group("start")), int(match.group("value"))


def recoverChrIndices(path):
    """"""
    result = []
    entryPattern = re.compile(r"^(?P<name>[^\n\t]+)\t(?P<start>[0-9]+)\t(?P<end>[0-9]+)\n?$")
    with open(os.path.join(path), "rt") as f:
        for line in f:
            match = entryPattern.match(line)
            result.append((match.group("end"), match.group("name")))

