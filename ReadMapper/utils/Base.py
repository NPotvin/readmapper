from enum import IntEnum

class Base(IntEnum):
    # X = 0;  END = 0
    A = 1;  a = 1
    C = 2;  c = 2
    G = 3;  g = 3
    T = 4;  t = 4
    N = 5;  n = 5

